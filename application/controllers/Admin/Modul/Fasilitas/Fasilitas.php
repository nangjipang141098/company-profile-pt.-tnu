<?php

class Fasilitas extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Fasilitas/M_Fasilitas', 'Fasilitas');
		$this->load->Model('Admin/Modul/Management/M_Management', 'Management');
		
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Fasilitas/Fasilitas',
					  'cek_query_edit' => $this->Fasilitas->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}



	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Fasilitas/Fasilitas'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/fasilitas/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$fasilitas_id				= $this->input->post('fasilitas_id');
				$fasilitas_nama				= $this->input->post('fasilitas_nama');
				$fasilitas_deskripsi		= $this->input->post('fasilitas_deskripsi');
				$status			= $this->input->post('status');
				$fasilitas_waktu			= date('Y-m=d');
				$config['image_library']='gd2';
	            $config['source_image']='./image/fasilitas/'.$gbr['file_name'];
	            $config['new_image']= './image/fasilitas/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'fasilitas_nama'		=> $fasilitas_nama,
					'fasilitas_deskripsi'	=> $fasilitas_deskripsi,
					'fasilitas_waktu'		=> $fasilitas_waktu,
					'status'				=> $status,
					'fasilitas_gambar'		=> $foto
				);
				$where = array(
					'fasilitas_id' => $fasilitas_id
				);
				$this->Fasilitas->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Fasilitas/Fasilitas');
			
			}
		}
		else{
			
				$fasilitas_id				= $this->input->post('fasilitas_id');
				$fasilitas_nama				= $this->input->post('fasilitas_nama');
				$fasilitas_deskripsi		= $this->input->post('fasilitas_deskripsi');
				$status						= $this->input->post('status');
				$fasilitas_waktu			= date('Y-m=d');	
			$data_user1 = array(
				'fasilitas_nama'		=> $fasilitas_nama,
				'fasilitas_deskripsi'	=> $fasilitas_deskripsi,
				'fasilitas_waktu'		=> $fasilitas_waktu,
				'status'				=> $status
					
			);
			$where = array(
				'fasilitas_id' => $fasilitas_id
			);
			$this->Fasilitas->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/Fasilitas/Fasilitas');
			
		}

	        
				
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Fasilitas->delete($id);
		redirect('Admin/Modul/Fasilitas/Fasilitas');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Fasilitas/Fasilitas'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/fasilitas/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$fasilitas_id				= $this->input->post('fasilitas_id');
				$fasilitas_nama				= $this->input->post('fasilitas_nama');
				$fasilitas_deskripsi		= $this->input->post('fasilitas_deskripsi');
				$status			= $this->input->post('status');
				$fasilitas_waktu			= date('Y-m=d');
				$config['image_library']='gd2';
	            $config['source_image']='./image/fasilitas/'.$gbr['file_name'];
	            $config['new_image']= './image/fasilitas/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'fasilitas_nama'		=> $fasilitas_nama,
					'fasilitas_deskripsi'	=> $fasilitas_deskripsi,
					'fasilitas_waktu'		=> $fasilitas_waktu,
					'status'		=> $status,
					'fasilitas_gambar'		=> $foto
				);
				$where = array(
					'fasilitas_id' => $fasilitas_id
				);
				$this->Fasilitas->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Fasilitas/Fasilitas');
			
			}
		}
		else{
			
			echo "<script>alert('Gambar wajib dimasukan')</script>";
			redirect('Admin/Modul/Fasilitas/Fasilitas');
			
		}

	        
				
	}


}