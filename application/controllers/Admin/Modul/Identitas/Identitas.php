<?php

class Identitas extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		$this->load->Model('Admin/Modul/Identitas/M_Identitas', 'Identitas');
		$this->load->Model('Admin/Modul/Management/M_Management', 'Management');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
	 	
	}
	
	public function index()
	{
		$data = array('title' => 'PT. TRI NINDYA UTAMA',
					  'content' => 'Admin/Modul/Identitas/Identitass'
                     );
                     
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}


	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Identitas/Identitass'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
		
		$config['upload_path'] = './image/management/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
				$identitas_id				= $this->input->get('ni');
				$identitas_website			= $this->input->post('identitas_website');
				$identitas_deskripsi		= $this->input->post('identitas_deskripsi');
				$identitas_keyword			= $this->input->post('identitas_keyword');
				$identitas_alamat			= $this->input->post('identitas_alamat');
				$identitas_notelp			= $this->input->post('identitas_notelp');
				$identitas_fb				= $this->input->post('identitas_fb');
				$identitas_email			= $this->input->post('identitas_email');
				$identitas_tw				= $this->input->post('identitas_tw');
				$identitas_notelp			= $this->input->post('identitas_notelp');
				$identitas_gp				= $this->input->post('identitas_gp');
				$identitas_yb				= $this->input->post('identitas_yb');
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$config['image_library']='gd2';
	            $config['source_image']='./image/identitas/'.$gbr['file_name'];
	            $config['new_image']= './image/identitas/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					'identitas_website'			=> $this->input->post('identitas_website'),
					'identitas_deskripsi'		=> $this->input->post('identitas_deskripsi'),
					'identitas_keyword'			=> $this->input->post('identitas_keyword'),
					'identitas_alamat'			=> $this->input->post('identitas_alamat'),
					'identitas_notelp'			=> $this->input->post('identitas_notelp'),
					'identitas_fb'				=> $this->input->post('identitas_fb'),
					'identitas_email'			=> $this->input->post('identitas_email'),
					'identitas_tw'				=> $this->input->post('identitas_tw'),
					'identitas_notelp'			=> $this->input->post('identitas_notelp'),
					'identitas_gp'				=> $this->input->post('identitas_gp'),
					'identitas_yb'				=> $this->input->post('identitas_yb'),
					'identitas_favicon'				=> $foto
				);
				$where = array(
					'identitas_id' => $this->input->post('identitas_id')
				);
				$this->Identitas->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Identitas/Identitas');
			
			}
		}
		
		else{
				 
			
				$data_user1 = array(
					'identitas_website'			=> $this->input->post('identitas_website'),
					'identitas_deskripsi'		=> $this->input->post('identitas_deskripsi'),
					'identitas_keyword'			=> $this->input->post('identitas_keyword'),
					'identitas_alamat'			=> $this->input->post('identitas_alamat'),
					'identitas_notelp'			=> $this->input->post('identitas_notelp'),
					'identitas_fb'				=> $this->input->post('identitas_fb'),
					'identitas_email'			=> $this->input->post('identitas_email'),
					'identitas_tw'				=> $this->input->post('identitas_tw'),
					'identitas_notelp'			=> $this->input->post('identitas_notelp'),
					'identitas_gp'				=> $this->input->post('identitas_gp'),
					'identitas_yb'				=> $this->input->post('identitas_yb')
				);
				$where = array(
					'identitas_id' => $this->input->get('ni')
				);
				$this->Identitas->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Identitas/Identitas');
		
			}
				
	}

	public function edit_identity(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Identitas/Identitass'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	

				$admin_user			= $this->input->post('admin_user');
				$admin_nama			= $this->input->post('admin_nama');
				$admin_email		= $this->input->post('admin_email');
				$admin_telepon		= $this->input->post('admin_telepon');
				$admin_level_kode	= $this->input->post('admin_level_kode');
				$admin_status		= $this->input->post('admin_status');
			
				$data_user1 = array(
					'admin_user'		=> $admin_user,
					'admin_nama'		=> $admin_nama,
					'admin_email'		=> $admin_email,
					'admin_telepon'		=> $admin_telepon,
					'admin_level_kode'	=> $admin_level_kode,
					'admin_status'		=> $admin_status
				);
				$where = array(
					'admin_user' => $admin_user
				);
				$this->Management->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Management/Management');
			        
				
	}


}