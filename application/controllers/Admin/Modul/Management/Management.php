<?php

class Management extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Management/M_Management', 'Management');
		
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Management/Management',
					  'cek_query_edit' => $this->Management->show_where($get),
					  'cek_query_admin_level' => $this->Management->level_code()
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Management/Management'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/management/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$admin_user			= $this->input->post('admin_user');
				$admin_nama			= $this->input->post('admin_nama');
				$admin_email		= $this->input->post('admin_email');
				$admin_telepon		= $this->input->post('admin_telepon');
				$admin_level_kode	= $this->input->post('admin_level_kode');
				$admin_status		= $this->input->post('admin_status');
				$config['image_library']='gd2';
	            $config['source_image']='./image/management/'.$gbr['file_name'];
	            $config['new_image']= './image/management/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					'admin_user'		=> $admin_user,
					'admin_nama'		=> $admin_nama,
					'admin_email'		=> $admin_email,
					'admin_telepon'		=> $admin_telepon,
					'admin_level_kode'	=> $admin_level_kode,
					'admin_status'		=> $admin_status,
					'foto'				=> $foto
				);
				$where = array(
					'admin_user' => $admin_user
				);
				$this->Management->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Management/Management');
			
			}
		}
		else{
			
			$admin_user			= $this->input->post('admin_user');
			$admin_nama			= $this->input->post('admin_nama');
			$admin_email		= $this->input->post('admin_email');
			$admin_telepon		= $this->input->post('admin_telepon');
			$admin_level_kode	= $this->input->post('admin_level_kode');
			$admin_status		= $this->input->post('admin_status');	
			$data_user1 = array(
					'admin_user'		=> $admin_user,
					'admin_nama'		=> $admin_nama,
					'admin_email'		=> $admin_email,
					'admin_telepon'		=> $admin_telepon,
					'admin_level_kode'	=> $admin_level_kode,
					'admin_status'		=> $admin_status,
					
			);
			$where = array(
				'admin_user' => $admin_user
			);
			$this->Management->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/Management/Management');
			
		}

	        
				
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Management->delete($id);
		redirect('Admin/Modul/Management/Management');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Management/Management'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/management/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$admin_user			= $this->input->post('admin_user');
				$admin_nama			= $this->input->post('admin_nama');
				$admin_email		= $this->input->post('admin_email');
				$admin_telepon		= $this->input->post('admin_telepon');
				$admin_level_kode	= $this->input->post('admin_level_kode');
				$admin_status		= $this->input->post('admin_status');
				$config['image_library']='gd2';
	            $config['source_image']='./image/management/'.$gbr['file_name'];
	            $config['new_image']= './image/management/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					'admin_user'		=> $admin_user,
					'admin_nama'		=> $admin_nama,
					'admin_email'		=> $admin_email,
					'admin_telepon'		=> $admin_telepon,
					'admin_level_kode'	=> $admin_level_kode,
					'admin_status'		=> $admin_status,
					'foto'				=> $foto
				);
				$where = array(
					'admin_user' => $admin_user
				);
				$this->Management->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Management/Management');
			
			}
		}
		else{
			echo "<script>alert('Wajib memasukan foto')</script>";
			redirect('Admin/Modul/Management/Management');
			
		}

	        
				
	}


}