<?php

class News extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/News/M_News', 'News');
		date_default_timezone_set('Asia/Jakarta');

	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/News/News',
					  'cek_query_edit' => $this->News->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/News/News'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/news/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$berita_id				= $this->input->post('berita_id');
				$berita_judul			= $this->input->post('berita_judul');
				$headline				= $this->input->post('headline');
				$berita_deskripsi		= $this->input->post('berita_deskripsi');
				$berita_waktu			= date('Y-m-d H:i:s');
				$berita_hits			= $this->input->post('berita_hits');
				$berita_tags			= $this->input->post('berita_tags');
				$kategori_id			= $this->input->post('kategori_id');
				$admin_nama				= $this->session->userdata('admin_nama');
                  

				
			
				$config['image_library']='gd2';
	            $config['source_image']='./image/news/'.$gbr['file_name'];
	            $config['new_image']= './image/news/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'berita_id'			=> $berita_id,
					'berita_judul'		=> $berita_judul,
					'headline'			=> $headline,
					'berita_deskripsi'	=> $berita_deskripsi,
					'berita_waktu'		=> $berita_waktu,
					'berita_hits'		=> $berita_hits,
					'berita_tags'		=> $berita_tags,
					'kategori_id'		=> $kategori_id,
					'admin_nama'		=> $admin_nama,
					'berita_gambar'		=> $foto
				);
				$where = array(
					'berita_id' => $berita_id
				);
				$this->News->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/News/News');
			
			}
		}
		else{
			$berita_id				= $this->input->post('berita_id');
			$berita_judul			= $this->input->post('berita_judul');
			$headline				= $this->input->post('headline');
			$berita_deskripsi		= $this->input->post('berita_deskripsi');
			$berita_waktu			= date('Y-m-d H:i:s');
			$berita_hits			= $this->input->post('berita_hits');
			$berita_tags			= $this->input->post('berita_tags');
			$kategori_id			= $this->input->post('kategori_id');
			$admin_nama				= $this->session->userdata('admin_nama');
				
			$data_user1 = array(
					
				'berita_id'			=> $berita_id,
				'berita_judul'		=> $berita_judul,
				'headline'			=> $headline,
				'berita_deskripsi'	=> $berita_deskripsi,
				'berita_waktu'		=> $berita_waktu,
				'berita_hits'		=> $berita_hits,
				'berita_tags'		=> $berita_tags,
				'kategori_id'		=> $kategori_id,
				'admin_nama'		=> $admin_nama
			);
			$where = array(
				'berita_id' => $berita_id
			);
			$this->News->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/News/News');
			
		}

	        
				
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->News->delete($id);
		redirect('Admin/Modul/News/News');
	}

	public function add(){
		
		// $data = array('title' => 'Admin TNU',
		// 			  'content' => 'Admin/Modul/News/News'
		// );
		// $this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/news/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$berita_judul			= $this->input->post('berita_judul');
				$headline				= $this->input->post('headline');
				$berita_deskripsi		= $this->input->post('berita_deskripsi');
				$berita_waktu			= date('Y-m-d H:i:s');
				$berita_hits			= $this->input->post('berita_hits');
				$berita_id				= $this->input->post('kategori_id');
				$berita_tags			= implode(' ', $this->input->post('berita_tags'));
				$admin_nama				= $this->session->userdata('admin_nama');

			
				

				//$for_query_berita = '';
				
				//$for_query_berita = substr($for_query_berita, 0, -2);
				//echo $for_query_berita;
				// $berita_tags			= implode($this->input->post('berita_tags'));
				$kategori_id			= $this->input->post('kategori_id');
				$admin_nama				= $this->session->userdata('admin_nama');
                  

				
			
				$config['image_library']='gd2';
	            $config['source_image']='./image/news/'.$gbr['file_name'];
	            $config['new_image']= './image/news/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'berita_judul'		=> $berita_judul,
					'headline'			=> $headline,
					'berita_deskripsi'	=> $berita_deskripsi,
					'berita_waktu'		=> $berita_waktu,
					'berita_hits'		=> $berita_hits,
					'berita_tags'		=> $berita_tags,
					'kategori_id'		=> $kategori_id,
					'admin_nama'		=> $admin_nama,
					'berita_gambar'		=> $foto
				);
				
				
				$this->News->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/News/News');
			
			}
		}
		else{
			$berita_judul			= $this->input->post('berita_judul');
			$headline				= $this->input->post('headline');
			$berita_deskripsi		= $this->input->post('berita_deskripsi');
			$berita_waktu			= date('Y-m-d H:i:s');
			$berita_hits			= $this->input->post('berita_hits');
			$berita_tags			=  implode($this->input->post('berita_tags'). ', ');
			$kategori_id			= $this->input->post('kategori_id');
			$admin_nama				= $this->session->userdata('admin_nama');
				
			$data_user1 = array(
					
			
				'berita_judul'		=> $berita_judul,
				'headline'			=> $headline,
				'berita_deskripsi'	=> $berita_deskripsi,
				'berita_waktu'		=> $berita_waktu,
				'berita_hits'		=> $berita_hits,
				'berita_tags'		=> $berita_tags,
				'kategori_id'		=> $kategori_id,
				'admin_nama'		=> $admin_nama
			);
		
			$this->News->create($data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/News/News');
			
		}

	        
				
	}


}