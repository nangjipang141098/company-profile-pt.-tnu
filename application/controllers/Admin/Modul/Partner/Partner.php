<?php

class Partner extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Partner/M_Partner', 'Partner');
		$this->load->Model('Admin/Modul/Produk/M_Produk', 'Produk');
	
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Partner/Partner',
					  'cek_query_edit' => $this->Partner->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function view_produk()
	{
		$get = $this->input->get('id');
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Partner/Jenis_produk',
					  'cek_query_data' => $this->Partner->show_where_id($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Partner->delete($id);
		redirect('Admin/Modul/Partner/Partner');
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Partner/Partner'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/partner/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$mitra_id				= $this->input->post('mitra_id');
				$mitra_nama				= $this->input->post('mitra_nama');
				$mitra_link				= $this->input->post('mitra_link');
				$jenis_produk			= $this->input->post('jenis_produk');
				$mitra_waktu			= date('Y-m-d');

				
				$config['image_library']='gd2';
	            $config['source_image']='./image/partner/'.$gbr['file_name'];
	            $config['new_image']= './image/partner/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
			
				$data_user1 = array(			
					'mitra_link'		=> $mitra_link,
					'mitra_waktu'		=> $mitra_waktu,
					'mitra_nama'		=> $mitra_nama,
					'jenis_produk'		=> $jenis_produk,
					'mitra_gambar'		=> $foto
				);
				$where = array(
					'mitra_id' => $mitra_id
				);
				
				$this->Partner->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Partner/Partner');
			
			}
		}
		else{
				$mitra_id				= $this->input->post('mitra_id');
				$mitra_nama				= $this->input->post('mitra_nama');
				$mitra_link				= $this->input->post('mitra_link');
				$jenis_produk			= $this->input->post('jenis_produk');
				$mitra_waktu			= date('Y-m-d');
				
			$data_user1 = array(			
				'mitra_link'		=> $mitra_link,
				'mitra_waktu'		=> $mitra_waktu,
				'mitra_nama'		=> $mitra_nama,
				'jenis_produk'		=> $jenis_produk
			);
			$where = array(
				'mitra_id' => $mitra_id
			);
			
			$this->Partner->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/Partner/Partner');
			
		}

	        
				
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Partner/Partner'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/partner/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$mitra_id				= $this->input->post('mitra_id');
				$mitra_nama				= $this->input->post('mitra_nama');
				$mitra_link				= $this->input->post('mitra_link');
				$jenis_produk			= $this->input->post('jenis_produk');
				$mitra_waktu			= date('Y-m-d');

				
				$config['image_library']='gd2';
	            $config['source_image']='./image/partner/'.$gbr['file_name'];
	            $config['new_image']= './image/partner/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
			
				$data_user1 = array(			
					'mitra_link'		=> $mitra_link,
					'mitra_waktu'		=> $mitra_waktu,
					'mitra_nama'		=> $mitra_nama,
					'jenis_produk'		=> $jenis_produk,
					'mitra_gambar'		=> $foto
				);
				
				$this->Partner->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Partner/Partner');
			
			}
		}
		else{
			
			echo "<script>alert('Gambar wajib dimasukan')</script>";
			redirect('Admin/Modul/Partner/Partner');
			
		}

	        
				
	}

	


}