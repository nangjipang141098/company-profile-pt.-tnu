<?php

class Produk extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Produk/M_Produk', 'Produk');
		
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Produk/Produk',
					  'cek_query_edit' => $this->Produk->show_where($get)
					//   'cek_query_admin_level' => $this->Produk->level_code()
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Produk/Produk'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/produk/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$id_produk			= $this->input->post('id_produk');
				$nama_produk		= $this->input->post('nama_produk');
				$deskripsi_indo		= $this->input->post('deskripsi_indo');
				$deskripsi_ing		= $this->input->post('deskripsi_ing');
				
				$admin_status		= $this->input->post('admin_status');
				$config['image_library']='gd2';
	            $config['source_image']='./image/produk/'.$gbr['file_name'];
	            $config['new_image']= './image/produk/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					'id_produk'			=> $id_produk,
					'nama_produk'		=> $nama_produk,
					'deskripsi_indo'	=> $deskripsi_indo,
					'deskripsi_ing'		=> $deskripsi_ing,
					'image'				=> $foto
				);
				$where = array(
					'id_produk' => $id_produk
				);
				$this->Produk->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Produk/Produk');
			
			}
		}
		else{
			
				$id_produk			= $this->input->post('id_produk');
				$nama_produk		= $this->input->post('nama_produk');
				$deskripsi_indo		= $this->input->post('deskripsi_indo');
				$deskripsi_ing		= $this->input->post('deskripsi_ing');	
				$data_user1 = array(
					'id_produk'			=> $id_produk,
					'nama_produk'		=> $nama_produk,
					'deskripsi_indo'	=> $deskripsi_indo,
					'deskripsi_ing'		=> $deskripsi_ing
				
				);
				$where = array(
					'id_produk' => $id_produk
				);
			$this->Produk->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/Produk/Produk');
			
		}

	        
				
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Produk->delete($id);
		redirect('Admin/Modul/Produk/Produk');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Produk/Produk'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$config['upload_path'] = './image/produk/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$id_produk			= $this->input->post('id_produk');
				$nama_produk		= $this->input->post('nama_produk');
				$deskripsi_indo		= $this->input->post('deskripsi_indo');
				$deskripsi_ing		= $this->input->post('deskripsi_ing');
				$config['image_library']='gd2';
	            $config['source_image']='./image/produk/'.$gbr['file_name'];
	            $config['new_image']= './image/produk/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					'id_produk'			=> $id_produk,
					'nama_produk'		=> $nama_produk,
					'deskripsi_indo'	=> $deskripsi_indo,
					'deskripsi_ing'		=> $deskripsi_ing,
					'image'				=> $foto
				);
				$this->Produk->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Produk/Produk');
			
			}
		}
		else{
			echo "<script>alert('Wajib memasukan foto')</script>";
			redirect('Admin/Modul/Produk/Produk');
			
		}

	        
				
	}


}