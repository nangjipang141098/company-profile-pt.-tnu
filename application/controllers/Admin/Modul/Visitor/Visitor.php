<?php
 
class Visitor extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Visitor/M_Visitor', 'Visitor');
	
	}
	
	public function index()
	{
		
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Visitor/Visitor',

	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	


}