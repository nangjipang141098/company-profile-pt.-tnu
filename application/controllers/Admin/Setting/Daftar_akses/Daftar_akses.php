<?php

class Daftar_akses extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Setting/Daftar_akses/M_Daftar_akses', 'Akses');
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_akses/Daftar_akses',
					  'cek_query_edit' => $this->Akses->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_akses/Daftar_akses'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

		$admin_level_kode			= $this->input->post('admin_level_kode');
		$admin_level_nama			= $this->input->post('admin_level_nama');
		$admin_level_status			= $this->input->post('admin_level_status');

				$data_user1 = array(			
					'admin_level_kode'		=> $admin_level_kode,
					'admin_level_nama'		=> $admin_level_nama,
					'admin_level_status'	=> $admin_level_status
				);
				$where = array(
					'admin_level_kode' => $admin_level_kode
				);	

				$this->Akses->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Setting/Daftar_akses/Daftar_akses');	
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Akses->delete($id);
		redirect('Admin/Setting/Daftar_akses/Daftar_akses');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_akses/Daftar_akses'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

				$admin_level_kode			= $this->input->post('admin_level_kode');
				$admin_level_nama			= $this->input->post('admin_level_nama');
				$admin_level_status			= $this->input->post('admin_level_status');

				$data_user1 = array(			
					'admin_level_kode'		=> $admin_level_kode,
					'admin_level_nama'		=> $admin_level_nama,
					'admin_level_status'	=> $admin_level_status
				);
				$where = array(
					'admin_level_kode' => $admin_level_kode
				);	
					
				$this->Akses->create($data_user1);
				echo "<script>alert('DATA BERHASIL DITAMBAH')</script>";
				redirect('Admin/Setting/Daftar_akses/Daftar_akses');	
	}
	

}