<?php
 
class Hak_akses extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Setting/Hak_akses/M_Hak_akses', 'HAkses');
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Hak_akses/Hak_akses',
					  'cek_query_edit' => $this->HAkses->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function get_menu()
	{
		
	   
		$menu_nama = $this->input->get('menu_nama');
			if($menu_nama !== ""){
				$query = $this->HAkses->get_menu_where($menu_nama);
				$output = '<option value="">--Pilih Menu--</option>';

				foreach($query->result_array() as $row){
					$output .= '<option value="'.$row["menu_nama"].'">'.$row["menu_nama"].'</option>';
				}
			}else{
				$output = '<option value="">--Tolong pilih data--</option>';
			}
			echo  $output;
	}

	public function menu(){
		$query_edit_level_code=$this->HAkses->level_code();
		$output = '<option value="">--Pilih Level--</option>';
            foreach ($query_edit_level_code->result_array() as $level_kode) {
				$output .= '<option value="'.$level_kode["admin_level_kode"].'">'.$level_kode["admin_level_kode"].'</option>';
				
			}
		
		echo $output;
                   
                      
	}

	
	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Hak_akses/Hak_akses'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

		$hak_akses_id			= $this->input->post('hak_akses_id');
		$deskripsi				= $this->input->post('deskripsi');
		$menu					= $this->input->post('admin_level_kode');
		$level_menu				= $this->input->post('level');

				$data_user1 = array(			
					'hak_akses_id'		=> $hak_akses_id,
					'deskripsi'			=> $deskripsi,
					'menu'				=> $menu,
					'level_menu'		=> $level_menu
				);
				$where = array(
					'hak_akses_id' 		=> $hak_akses_id
				);	

				$this->HAkses->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Setting/Hak_akses/Hak_akses');	
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->HAkses->delete($id);
		redirect('Admin/Setting/Hak_akses/Hak_akses');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Hak_akses/Hak_akses'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

		$hak_akses_id			= $this->input->post('hak_akses_id');
		$deskripsi				= $this->input->post('deskripsi');
		$menu					= $this->input->post('level');
		$level_menu				= $this->input->post('menu');

		$data_user1 = array(			
			'hak_akses_id'		=> $hak_akses_id,
			'deskripsi'			=> $deskripsi,
			'menu'				=> $menu,
			'level_menu'		=> $level_menu
		);
		

					
				$this->HAkses->create($data_user1);
				echo "<script>alert('DATA BERHASIL DITAMBAH')</script>";
				redirect('Admin/Setting/Hak_akses/Hak_akses');	
	}
	

}