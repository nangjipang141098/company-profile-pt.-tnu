<?php

class Galeri_video extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Tools/Galeri_video/M_Galeri_video', 'Video');
		
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Galeri_video/Galeri_video',
					  'cek_query_edit' => $this->Video->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Galeri_video/Galeri_video'
		);
		
				$video_id				= $this->input->post('video_id');
				$video_judul			= $this->input->post('video_judul');
				$video_deskripsi		= $this->input->post('video_deskripsi');
				$video_link				= $this->input->post('video_link');
				$video_waktu			= date('Y-m-d');

				$data_user1 = array(
					'video_id'			=> $video_id,
					'video_judul'		=> $video_judul,
					'video_deskripsi'	=> $video_deskripsi,
					'video_link'		=> $video_link,
					'video_waktu'		=> $video_waktu
				);
				$where = array(
					'video_id' => $video_id
				);
				$this->Video->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Tools/Galeri_video/Galeri_video');
			
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Galeri_video/Galeri_video'
		);
	
				$video_id				= $this->input->post('video_id');
				$video_judul			= $this->input->post('video_judul');
				$video_deskripsi		= $this->input->post('video_deskripsi');
				$video_link				= $this->input->post('video_link');
				$video_waktu			= date('Y-m-d');

				$data_user1 = array(
					'video_id'			=> $video_id,
					'video_judul'		=> $video_judul,
					'video_deskripsi'	=> $video_deskripsi,
					'video_link'		=> $video_link,
					'video_waktu'		=> $video_waktu
				);
				
				$this->Video->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Tools/Galeri_video/Galeri_video');
			
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Video->delete($id);
		redirect('Admin/Tools/Galeri_video/Galeri_video');
	}

	

}