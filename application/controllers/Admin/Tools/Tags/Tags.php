<?php

class Tags extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Tools/Tags/M_Tags', 'Tags');
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Tags/Tags',
					  'cek_query_edit' => $this->Tags->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Tags/Tags'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

				$tag_id				= $this->input->post('tag_id');
				$tag_judul			= $this->input->post('tag_judul');
				$tag_seo			= $this->input->post('tag_seo');

				$data_user1 = array(
					
					'tag_id'		=> $tag_id,
					'tag_judul'		=> $tag_judul,
					'tag_seo'		=> $tag_seo
				);
				$where = array(
					'tag_id' => $tag_id
				);	
					
				$this->Tags->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Tools/Tags/Tags');	
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Tags->delete($id);
		redirect('Admin/Tools/Tags/Tags');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Tools/Tags/Tags'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

				$tag_id				= $this->input->post('tag_id');
				$tag_judul			= $this->input->post('tag_judul');
				$tag_seo			= $this->input->post('tag_seo');

				$data_user1 = array(			
					'tag_id'		=> $tag_id,
					'tag_judul'		=> $tag_judul,
					'tag_seo'		=> $tag_seo
				);
				$where = array(
					'tag_id' => $tag_id
				);	
					
				$this->Tags->creare($data_user1);
				echo "<script>alert('DATA BERHASIL DITAMBAH')</script>";
				redirect('Admin/Tools/Tags/Tags');	
	}
	

}