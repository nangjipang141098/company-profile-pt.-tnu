<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->Model('Admin/Modul/Visitor/M_Visitor', 'Visitor');
		$this->load->Model('Admin/Modul/Produk/M_Produk', 'Produk'); 
		$this->load->Model('Admin/Modul/Partner/M_Partner', 'Partner');
		$this->load->Model('Admin/Modul/Fasilitas/M_Fasilitas', 'Fasilitas');
		$this->load->Model('Admin/Modul/News/M_News', 'News');
		
		$this->load->library('user_agent');
		$this->load->library('phpmailer_lib');
    }

	public function index()
	{
		$get = $this->input->get('id');


	$this->load->language('dashboard',$get);

	// $data['judul'] = $this->lang->line('title');
	// $data['isinya'] = $this->lang->line('content');

	
	$data['title'] = $this->lang->line('title');
	$data['home'] = $this->lang->line('home');
	$data['About'] = $this->lang->line('About');
	$data['Services'] = $this->lang->line('Services');
	$data['Portfolio'] = $this->lang->line('Portfolio');
	$data['Testimoni'] = $this->lang->line('Testimoni');
	$data['Team'] = $this->lang->line('Team');
	$data['Galery'] = $this->lang->line('Galery');
	$data['Drop_down'] = $this->lang->line('Drop_down');
	$data['contact'] = $this->lang->line('contact');
	$data['wellcome'] = $this->lang->line('wellcome');
	$data['wellcome2'] = $this->lang->line('wellcome2');
	$data['video_company'] = $this->lang->line('video_company');
	$data['about_us'] = $this->lang->line('about_us');
	$data['title_about_us'] = $this->lang->line('title_about_us');
	$data['description_about_us'] = $this->lang->line('description_about_us');
	$data['title_vision_mision'] = $this->lang->line('title_vision_mision');
	$data['title_vision'] = $this->lang->line('title_vision');
	$data['description_vision'] = $this->lang->line('description_vision');
	$data['title_mision'] = $this->lang->line('title_mision');
	$data['description_mision'] = $this->lang->line('description_mision');
	$data['Drop_down_corporate'] = $this->lang->line('Drop_down_corporate'); 
	$data['Drop_down_pns'] = $this->lang->line('Drop_down_pns');
	$data['Drop_down_article_media'] = $this->lang->line('Drop_down_article_media'); 

	$data['services'] = $this->lang->line('services');
	$data['app_development'] = $this->lang->line('app_development');
	$data['type_app_develompemnt'] = $this->lang->line('type_app_develompemnt');
	$data['infrastructure_support'] = $this->lang->line('infrastructure_support');
	$data['type_infrastructure_support'] = $this->lang->line('type_infrastructure_support');
	$data['staffing_service_models'] = $this->lang->line('staffing_service_models');
	$data['type_staffing_service_models'] = $this->lang->line('type_staffing_service_models');
	$data['portfolio'] = $this->lang->line('portfolio');
	$data['show_all'] = $this->lang->line('show_all');
	$data['team_description'] = $this->lang->line('team_description');
	$data['latest_update_section'] = $this->lang->line('latest_update_section');
	$data['up_coming_event_section'] = $this->lang->line('up_coming_event_section');

	$data['description_etila'] = $this->lang->line('description_etila');
	$data['description_erisk'] = $this->lang->line('description_erisk');
	$data['description_wbs'] = $this->lang->line('description_wbs');
	$data['description_kms'] = $this->lang->line('description_kms');
	$data['description_eoffice'] = $this->lang->line('description_eoffice');
	$data['description_kppu'] = $this->lang->line('description_kppu');;
	$data['description_upg'] = $this->lang->line('description_upg');

	$data['request_documentation'] = $this->lang->line('request_documentation');
	$data['description_request_documentation'] = $this->lang->line('description_request_documentation');

	$data['Career'] =  $this->lang->line('Career');;
	$this->load->view('Public/index', $data);
	}

	public function galery_page()
	{
		$get = $this->input->get('id');


	$this->load->language('dashboard',$get);

	// $data['judul'] = $this->lang->line('title');
	// $data['isinya'] = $this->lang->line('content');

	
	$data['title'] = $this->lang->line('title');
	$data['home'] = $this->lang->line('home');
	$data['About'] = $this->lang->line('About');
	$data['Services'] = $this->lang->line('Services');
	$data['Portfolio'] = $this->lang->line('Portfolio');
	$data['Testimoni'] = $this->lang->line('Testimoni');
	$data['Team'] = $this->lang->line('Team');
	$data['Galery'] = $this->lang->line('Galery');
	$data['Drop_down'] = $this->lang->line('Drop_down');
	$data['contact'] = $this->lang->line('contact');
	$data['wellcome'] = $this->lang->line('wellcome');
	$data['wellcome2'] = $this->lang->line('wellcome2');
	$data['video_company'] = $this->lang->line('video_company');
	$data['about_us'] = $this->lang->line('about_us');
	$data['title_about_us'] = $this->lang->line('title_about_us');
	$data['description_about_us'] = $this->lang->line('description_about_us');
	$data['title_vision_mision'] = $this->lang->line('title_vision_mision');
	$data['title_vision'] = $this->lang->line('title_vision');
	$data['description_vision'] = $this->lang->line('description_vision');
	$data['title_mision'] = $this->lang->line('title_mision');
	$data['description_mision'] = $this->lang->line('description_mision');
	$data['Drop_down_corporate'] = $this->lang->line('Drop_down_corporate'); 
	$data['Drop_down_pns'] = $this->lang->line('Drop_down_pns');
	$data['Drop_down_article_media'] = $this->lang->line('Drop_down_article_media'); 

	$data['services'] = $this->lang->line('services');
	$data['app_development'] = $this->lang->line('app_development');
	$data['type_app_develompemnt'] = $this->lang->line('type_app_develompemnt');
	$data['infrastructure_support'] = $this->lang->line('infrastructure_support');
	$data['type_infrastructure_support'] = $this->lang->line('type_infrastructure_support');
	$data['staffing_service_models'] = $this->lang->line('staffing_service_models');
	$data['type_staffing_service_models'] = $this->lang->line('type_staffing_service_models');
	$data['portfolio'] = $this->lang->line('portfolio');
	$data['show_all'] = $this->lang->line('show_all');
	$data['team_description'] = $this->lang->line('team_description');
	$data['latest_update_section'] = $this->lang->line('latest_update_section');
	$data['up_coming_event_section'] = $this->lang->line('up_coming_event_section');

	$data['description_etila'] = $this->lang->line('description_etila');
	$data['description_erisk'] = $this->lang->line('description_erisk');
	$data['description_wbs'] = $this->lang->line('description_wbs');
	$data['description_kms'] = $this->lang->line('description_kms');
	$data['description_eoffice'] = $this->lang->line('description_eoffice');
	$data['description_kppu'] = $this->lang->line('description_kppu');;
	$data['description_upg'] = $this->lang->line('description_upg');

	$data['request_documentation'] = $this->lang->line('request_documentation');
	$data['description_request_documentation'] = $this->lang->line('description_request_documentation');

	$data['Career'] =  $this->lang->line('Career');;
	$this->load->view('Public/galeri_page', $data);
	}


	function send_mail() { 

		$from_email = "nangjipang@gmail.com"; 
		$to_email = $this->input->post('email'); 
		$nama = $this->input->post('nama'); 
		$aplikasi  = implode(' ', $this->input->post('select_request_documentation'));
		$massage = $this->input->post('massage'); 
		$pesan = "Nama " . $nama . "Permintaan request demo aplikasi ". $aplikasi ." ".$massage;

		$config = Array(
			   'protocol' => 'smtp',
			   'smtp_host' => 'ssl://smtp.googlemail.com',
			   'smtp_port' => 465,
			   'smtp_user' => $from_email,
			   'smtp_pass' => 'xxx',
			   'mailtype'  => 'html', 
			   'charset'   => 'iso-8859-1'
	    );

		   $this->load->library('email', $config);
		   $this->email->set_newline("\r\n");   

		$this->email->from($from_email, 'Danang Aji Pangestu'); 
		$this->email->to($to_email);
		$this->email->subject('Request Demo Applikasi'); 
		$this->email->message($pesan); 

		//Send mail 
		if($this->email->send()){
			   echo ("Email berhasil terkirim."); 
		}else {
			   echo ("Email gagal dikirim."); 
			
		} 
	 }
   

}
