<?php


class Login extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Login/M_Login', 'login_user');
		$this->load->library('session');
		$this->load->library('upload');
        $this->load->helper('url');
        $this->load->helper('form');
    }

    public function login()
    {
        $this->form_validation->set_rules('admin_user','admin_user','required',['required' => 'Username wajib diisi!']);
        $this->form_validation->set_rules('admin_pass','admin_pass','required',['required' => 'Password wajib diisi!']);
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Login/login');
        }else{
            $auth = $this->login_user->cek_login();
            //$auth1 = $this->model_auth->status();
            $status_karyawan = $auth->admin_status;
     
            if($auth == FALSE){
                $this->session->set_flashdata('pesan','<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Username atau Password Anda Salah
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>');
                    redirect('Login/Login/login/');
            }
            
            else{
                $this->session->set_userdata('admin_user',$auth->admin_user);
                $this->session->set_userdata('admin_level',$auth->admin_level_code);
                $this->session->set_userdata('admin_nama',$auth->admin_nama);
                $this->session->set_userdata('admin_status',$auth->admin_status);
                $status_karyawan    = $auth->admin_status;
                $auth               = $this->login_user->cek_login();
                $cek_query          = $this->login_user->check_user('admin_user');     
                $data = $cek_query->row_array();
                
                
                    if($status_karyawan=="A"){
                        redirect('Admin/Dashboard/Dashboard');
                    }
                   
                    else{
                        $this->session->set_flashdata('pesan','<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        Akun Anda DiNoneAktifkan
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>');
               
                        redirect('Login/Login/login/');
                    }
                    

                    // switch($auth1->role_id){
                    // case 1 :
                    //     if(($status_karyawan='AKTIF'))   { redirect('manajemen/dashboard_manajemen');}   
                    //     if(($status_karyawan='NONE')){ 
                    //         $this->session->set_flashdata('pesan1','<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    //     DINONAKTIFKAN
                    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    //     <span aria-hidden="true">&times;</span>
                    //     </button>
                    // </div>');
                    // redirect('auth/login');
                    //      }  
                    //     break;
                    // case 2 : 
                    //     if(($status_karyawan='AKTIF'))   { redirect('hrd/dashboard_hrd');}   
                    //     if(($status_karyawan='NONE')){
                    //         $this->session->set_flashdata('pesan1','<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    //     DINONAKTIFKAN
                    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    //     <span aria-hidden="true">&times;</span>
                    //     </button>
                    // </div>');
                    // redirect('auth/login');
                    //      }
                    //     break;
                    // case 3 : 
                    //     if(($status_karyawan='AKTIF'))   { redirect('user/dashboard_user');}   
                    //     if(($status_karyawan='NONE')){
                    //         $this->session->set_flashdata('pesan1','<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    //     DINONAKTIFKAN
                    //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    //     <span aria-hidden="true">&times;</span>
                    //     </button>
                    // </div>');
                    // redirect('auth/login');
                    //     }
                    //     break;
                    // default:
                    //     break;
               // }

                
            }
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth/login');
    }
}