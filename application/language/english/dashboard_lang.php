<?php
$lang['title'] = 'PT. TRI NINDYA UTAMA';
$lang['home'] = 'Home';
$lang['About'] = 'About';
$lang['Services'] = 'Services';
$lang['Portfolio'] = 'Product';
$lang['Galery'] = 'Galery';
$lang['Testimoni'] = 'Testimonials';
$lang['Team'] = 'Team';
$lang['Drop_down'] = 'Language';
$lang['Drop_down_corporate'] = "Corporate"; 
$lang['Drop_down_pns'] = "Service and Product "; 
$lang['Drop_down_article_media'] = "Articles and Media"; 

$lang['contact'] = 'Contact';


$lang['wellcome'] = 'Welcome To Bangga Solution';
$lang['wellcome2'] = 'PT. Tri Nindya Utama provides a broad portfolio of information <br> technology solutions and business process to its clients';


$lang['video_company'] = 'Video Company';

$lang['about_us'] = 'About Us';
$lang['title_about_us'] = 'This is a description about us';
$lang['description_about_us'] = 'PT. TRI NINDYA UTAMA or better known as SOLUSI BANGGA is a provider of various Information Technology (IT) solutions and business processes. Established in 2013, as a Software Developer, BANGGA SOLUTIONS has developed various applications, especially the Audit Management System (AMS) and the Risk Management System (RMS). Manned by a professional and experienced workforce in their fields (Programmers, IT Support), BANGGA SOLUTIONS also serves a wide range of application development services, IT infrastructure implementation (Network, Hardware), website development and maintenance, and spare parts maintenance. computer. With our experience, we offer the best solutions for your business and organization with reliable support';


$lang['title_vision_mision'] = 'Vision & Mision';
$lang['title_vision'] = 'Vision';
$lang['description_vision'] = 'Description Vision';
$lang['title_mision'] = 'Mision';
$lang['description_mision'] = 'Description Vision';

$lang['services'] = 'Services';
$lang['app_development'] = 'Application Development';
$lang['type_app_develompemnt'] = ' <b> Custom Application Development, Web Application Development, Application Support & Maintenance, Application Testing, Application Migration, Web Design and Maintenance </b>';
$lang['infrastructure_support'] = 'Infrastructure Support';
$lang['type_infrastructure_support'] = ' <b> Asset/ Hardware Management Services, Network Management Services, Server Management Services, Mail Management Services  </b>'; 
$lang['staffing_service_models'] = 'Staffing Service Models';
$lang['type_staffing_service_models'] = ' <b> Temporary Staffing (Short Term), Permanent Staffing, Long Term Contract Staffing , Project Staffing  </b>';

// $lang['services'] = 'Services';
// $lang['app_development'] = 'Application Development';
// $lang['type_app_develompemnt'] = ' <center>
// • Custom Application Development • <br>
// • Web Application Development • <br>
// • Application Support & Maintenance • <br>
// • Application Testing • <br>
// • Application Migration • <br>
// • Web Design and Maintenance • </center>';
// $lang['infrastructure_support'] = 'Infrastructure Support';
// $lang['type_infrastructure_support'] = ' <center>
// • Asset/ Hardware Management Services • <br>
// • Network Management Services • <br>
// • Server Management Services • <br>
// • Mail Management Services • </center>'; 
// $lang['staffing_service_models'] = 'Staffing Service Models';
// $lang['type_staffing_service_models'] = ' <center>
// • Temporary Staffing (Short Term) • <br>
// • Permanent Staffing • <br>
// • Long Term Contract Staffing • <br>
// • Project Staffing •  </center>';

$lang['portfolio'] = 'Portfolio';
$lang['show_all'] = 'Show All';

$lang['team_description'] = 'These are the management of PT. TRI NINDYA UTAMA';

$lang['latest_update_section'] = 'Latest Update Section';
$lang['up_coming_event_section'] = 'Up coming Event Section';
$lang['Career'] = 'Career';

$lang['description_etila'] = 'Audit management application that manages planning, implementation, reporting and monitoring of audit results follow-up activities';
$lang['description_erisk'] = 'Risk management application that manages the activities of identification, assessment, mitigation plans and monitoring of risk mitigation';
$lang['description_wbs'] = 'An application that manages complaints from internal or external parties to the status of the follow-up of complaints';
$lang['description_kms'] = 'Application systems that facilitate knowledge capture, storage, search, sharing and reuse';
$lang['description_eoffice'] = 'Application for managing incoming, outgoing mail and letter disposition as a form of digitizing the correspondence process';
$lang['description_kppu'] = '';
$lang['description_upg'] = '';


$lang['request_documentation'] = 'Request Documentation';
$lang['description_request_documentation'] = 'Please insert your email if you wish to request a demonstration of the application';
?>