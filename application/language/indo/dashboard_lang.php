<?php
$lang['title'] = 'PT. TRI NINDYA UTAMA';
$lang['home'] = 'Utama';
$lang['About'] = 'Tentang';
$lang['Services'] = 'Layanan';
$lang['Portfolio'] = 'Produk';
$lang['Galery'] = 'Galeri';
$lang['Testimoni'] = 'Testimoni';
$lang['Team'] = 'Anggota';
$lang['Drop_down'] = 'Bahasa';
$lang['contact'] = 'Kontak';
$lang['Drop_down_corporate'] = "Perusahaan"; 
$lang['Drop_down_pns'] = "Layanan dan Produk "; 
$lang['Drop_down_article_media'] = "Artikel dan Media"; 


$lang['wellcome'] = 'Selamat Datang Di Bangga Solusution';
$lang['wellcome2'] = 'PT. Tri Nindya Utama menyediakan portofolio yang luas dari <br> solusi teknologi informasi dan proses bisnis kepada kliennya';


$lang['video_company'] = 'Video Perusahaan';

$lang['about_us'] = 'Tentang Kami';
$lang['title_about_us'] = 'Berikut merupakan deskripsi singkat tentang kami';
$lang['description_about_us'] = 'PT. TRI NINDYA UTAMA atau yang lebih dikenal dengan BANGGA SOLUTIONS adalah sebuah provider penyedia berbagai macam solusi Teknologi Informasi (TI) dan bisnis proses. Berdiri sejak tahun 2013, sebagai sebuah Software Developer, BANGGA SOLUTIONS sudah mengembangkan berbagai macam aplikasi, khususnya Audit Management System (AMS) dan Risk Management System (RMS). Dengan di awaki oleh para tenaga kerja profesional dan berpengalaman di bidangnya (Programmer, IT Support), BANGGA SOLUTIONS juga melayani berbagai macam jasa pengembangan aplikasi terkini, penerapan infrastruktur IT (Jaringan, Piranti Keras), pengembangan dan perawatan website perusahaan, dan perawatan suku cadang komputer. Dengan pengalaman yang dimiliki, kami menawarkan solusi terbaik untuk bisnis dan organisasi anda dengan layanan support yang dapat diandalkan';


$lang['title_vision_mision'] = 'Visi & Misi';
$lang['title_vision'] = 'Visi';
$lang['description_vision'] = 'Deskripsi Visi';
$lang['title_mision'] = 'Misi';
$lang['description_mision'] = 'Deskripsi Visi';



$lang['services'] = 'Layanan';
$lang['app_development'] = 'Pengembangan Aplikasi';
$lang['type_app_develompemnt'] = '<b>Pengembangan Aplikasi Kustom, Pengembangan Aplikasi Web, Dukungan & Pemeliharaan Aplikasi, Pengujian Aplikasi, Migrasi Aplikasi, Desain dan Pemeliharaan Web </b>';
$lang['infrastructure_support'] = 'Dukungan Infrastruktur';
$lang['type_infrastructure_support'] = '<b> Layanan Manajemen Aset / Perangkat Keras, Layanan Manajemen Jaringan, Layanan Manajemen Server, Layanan Manajemen Surat</b>'; 
$lang['staffing_service_models'] = 'Model Layanan Kepegawaian';
$lang['type_staffing_service_models'] = ' <b> Staf Sementara (Jangka Pendek), Staf Tetap, Staf Kontrak Jangka Panjang, Staf Proyek </b>';


// $lang['services'] = 'Layanan';
// $lang['app_development'] = 'Pengembangan Aplikasi';
// $lang['type_app_develompemnt'] = '<center>
// • Pengembangan Aplikasi Kustom • <br>
// • Pengembangan Aplikasi Web • <br>
// • Dukungan & Pemeliharaan Aplikasi • <br>
// • Pengujian Aplikasi • <br>
// • Migrasi Aplikasi • <br>
// • Desain dan Pemeliharaan Web • </center>';
// $lang['infrastructure_support'] = 'Dukungan Infrastruktur';
// $lang['type_infrastructure_support'] = '<center>
// • Layanan Manajemen Aset / Perangkat Keras • <br>
// • Layanan Manajemen Jaringan • <br>
// • Layanan Manajemen Server • <br>
// • Layanan Manajemen Surat •</center>'; 
// $lang['staffing_service_models'] = 'Model Layanan Kepegawaian';
// $lang['type_staffing_service_models'] = ' <center>
// • Staf Sementara (Jangka Pendek) • <br>
// • Staf Tetap • <br>
// • Staf Kontrak Jangka Panjang • <br>
// • Staf Proyek • </center>';

$lang['portfolio'] = 'Portofolio';
$lang['show_all'] = 'Tampilkan Semua';

$lang['team_description'] = 'Berikut merupakan jajaran dari PT. TRI NINDYA UTAMA';

$lang['latest_update_section'] = 'Kabar Terbaru';
$lang['up_coming_event_section'] = 'Acara Mendatang';
$lang['Career'] = 'Pekerjaan';

$lang['description_etila'] = 'Aplikasi manajemen audit yang mengelola aktivitas perencanaan, pelaksanaan, pelaporan dan monitoring tindak lanjut hasil audit';
$lang['description_erisk'] = 'Aplikasi manajemen risiko yang mengelola aktivitas identifikasi, penilaian, rencana mitigasi dan monitoring mitigasi risiko';
$lang['description_wbs'] = 'Aplikasi yang mengelola pengaduan dari pihak internal atau eksternal sampai dengan status tindak lanjut pengaduan';
$lang['description_kms'] = 'Sistem aplikasi yang memfasilitasi menangkap, menyimpan, mencari, berbagi dan menggunakan kembali pengetahuan (knowledge)';
$lang['description_eoffice'] = 'Aplikasi pengelolaan surat masuk, surat keluar dan disposisi surat sebagai wujud digitalisasi proses surat menyurat';
$lang['description_kppu'] = '';
$lang['description_upg'] = '';



$lang['request_documentation'] = 'Pengajuan Doukumentasi';
$lang['description_request_documentation'] = 'Silahkan isi form dibawah jika ingin mengajukan dokumentasi dari aplikasi'
?>