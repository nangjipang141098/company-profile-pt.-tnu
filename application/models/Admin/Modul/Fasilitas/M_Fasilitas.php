<?php

class M_Fasilitas extends CI_Model{

    protected $table = "fasilitas";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_limit(){
        $query = $this->db->query("SELECT * FROM $this->table ORDER BY fasilitas_waktu ASC LIMIT 0,8");
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('fasilitas_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('fasilitas_id', $where);
		$this->db->delete($this->table);
    }


  

    
}