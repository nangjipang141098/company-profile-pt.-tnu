<?php

class M_News extends CI_Model{

    protected $table = "berita";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('berita_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function show_where_jobs($start, $limit){
		$query = $this->db->query("SELECT * FROM $this->table WHERE NOT kategori_id=4 ORDER BY kategori_id ASC LIMIT $start, $limit");
        return $query;
    }

    function show_where_news($start, $limit){
		$query = $this->db->query("SELECT * FROM $this->table WHERE kategori_id=4 ORDER BY kategori_id ASC LIMIT $start, $limit");
        return $query;
    }

    

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('berita_id', $where);
		$this->db->delete($this->table);
    }

    function show_kategori(){
        $this->db->select('*');
        $this->db->from('kategori');
		$query = $this->db->get();
        return $query;
    }

    function show_tags(){
        $this->db->select('*');
		$this->db->from('tags');
		$query = $this->db->get();
        return $query;
    }


  

    
}