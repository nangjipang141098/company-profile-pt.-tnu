<?php

class M_Produk extends CI_Model{

    protected $table = "produk";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id_produk', $where);
		$query = $this->db->get();
        return $query;
    }

    function show_where_id($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('jenis_produk', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('id_produk', $where);
		$this->db->delete($this->table);
    }


    //akses level code pada modal edit
    function level_code()
	{
		$this->db->select('*');
        $this->db->from('admin_level');
  //      $this->db->where('admin_level_kode', $where);
		$query = $this->db->get();
        return $query;
    }

    //status activaction account
    function admin_status()
	{
		$this->db->select('*');
        $this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    
}