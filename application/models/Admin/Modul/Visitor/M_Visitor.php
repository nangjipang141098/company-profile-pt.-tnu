<?php

class M_Visitor extends CI_Model{
 
    protected $table = "visitor";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    
    
    function delete($where)
	{
		$this->db->where('mitra_id', $where);
		$this->db->delete($this->table);
    }


  

    
}