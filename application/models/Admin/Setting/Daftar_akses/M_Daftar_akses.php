<?php

class M_Daftar_akses extends CI_Model{
 
    protected $table = "admin_level";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('admin_level_kode', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('admin_level_kode', $where);
		$this->db->delete($this->table);
    }

    //akses level code pada modal edit
    function level_code($where)
	{
		$this->db->select('*');
        $this->db->from('admin_level');
        $this->db->where('admin_level_kode', $where);
		$query = $this->db->get();
        return $query;
    }

    //status activaction account
    function admin_status()
	{
		$this->db->select('*');
        $this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

  

    
}