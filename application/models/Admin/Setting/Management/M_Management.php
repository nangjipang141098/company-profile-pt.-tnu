<?php

class M_Management extends CI_Model{

    protected $table = "admin";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('admin_user', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('admin_user', $where);
		$this->db->delete($this->table);
    }


    //akses level code pada modal edit
    function level_code()
	{
		$this->db->select('*');
		$this->db->from('admin_level');
		$query = $this->db->get();
        return $query;
    }

    //status activaction account
    function admin_status()
	{
		$this->db->select('*');
        $this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    
}