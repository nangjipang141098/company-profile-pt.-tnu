<?php

class M_Galeri_video extends CI_Model{

    protected $table = "galeri_video";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('video_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('video_id', $where);
		$this->db->delete($this->table);
    }


  

    
}