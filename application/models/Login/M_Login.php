<?php

class M_Login extends CI_Model{

    protected $table = "admin";

    public function cek_login()
    {
        $admin_user     = set_value('admin_user');
        $admin_pass     = set_value('admin_pass');
        $result         = $this->db->where('admin_user',$admin_user)
                                    ->where('admin_pass',$admin_pass)
                                    ->limit(1)
                                    ->get($this->table);
        if($result->num_rows() > 0){
            return $result->row();
        }else{
            return array();
        }
    }

    public function check_user($admin_user){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('admin_user', $admin_user);
		$query = $this->db->get();
        return $query;
	}

    
}