<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Fasilitas<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Fasilitas->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak($row['fasilitas_nama']) ?> </td>
                    <td><?php cetak($row['fasilitas_deskripsi']) ?> </td>
                    <td><?php cetak($row['fasilitas_waktu']) ?> </td>
                    <td><?php cetak($row['status']) ?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>"></td>

                    <td>
                      <a href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete?id=') . $row['fasilitas_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['fasilitas_id'] )?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['fasilitas_id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                              
                                $cek_query_edit=$this->Fasilitas->show_where($row['fasilitas_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Fasilitas/Fasilitas/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">
                                  
                                    <label>Username</label>
                                    <input type="text" name="fasilitas_id" class="form-control" value="<?php cetak( $edit['fasilitas_id'] )?>" placeholder="Id Karyawan" required>

                                    <label>Nama</label>
                                    <input type="text" name="fasilitas_nama" class="form-control" value="<?php cetak( $edit['fasilitas_nama'] )?>" placeholder="Id Klaim" required>

                                    <label>Email</label>
                                    <input type="text" name="fasilitas_deskripsi" class="form-control" value="<?php cetak( $edit['fasilitas_deskripsi'] )?>" placeholder="Jumlah Diajukan" required>
                                    
                                    <label>Telepon</label>
                                    <input type="text" name="admin_telepon" class="form-control" value="<?php cetak( $edit['fasilitas_waktu'] )?>" placeholder="Jumlah Diajukan" required>

                                    <label>Status</label><br>
                                    <input type="radio" name="status"  value="1" <?php cetak( ($row['status'] == '1') ? "checked": "" )?> > TRUE <br>
                                    <input type="radio" name="status" value="0" <?php cetak( ($row['status'] == '0') ? "checked": "" )?> > FALSE </td>

                                  
                                    <br><br>
                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak(base_url())?>Admin/Modul/Fasilitas/Fasilitas/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
          
          <div class="form-group">
                                  
              <label>Username</label>
              <input type="text" name="fasilitas_id" class="form-control"  placeholder="Fasilitas" required>

              <label>Nama</label>
              <input type="text" name="fasilitas_nama" class="form-control"  placeholder="Fasility Name" required>

              <label>Email</label>
              <input type="text" name="fasilitas_deskripsi" class="form-control"  placeholder="Description" required>
                                  
              <label>Telepon</label>
              <input type="text" name="admin_telepon" class="form-control"  placeholder="Jumlah Diajukan" required>

              <label>Status</label><br>
              <input type="radio" name="status"  value="1"  > TRUE <br>
              <input type="radio" name="status" value="0"  > FALSE </td>

                                
              <br><br>
              <label> Foto </label>  
              <br>
              <input type="file" class="form-control" name="filefoto" >
              <br>

                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>