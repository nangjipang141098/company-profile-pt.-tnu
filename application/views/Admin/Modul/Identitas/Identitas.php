

<!-- Content Row -->
<div class="row">

  



<!-- Content Row -->

<div class="row">

  <!-- Area Chart -->
  <div class="col-xl-8 col-lg-7">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown -->
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-warning"><?= $title ?></h6>
        <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>

        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          
        </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="chart-area">
          <?php 
          $query = $this->Identitas->show();
          foreach($query->result_array() as $row): 
          ?>
            <!-- Color System -->
                <div class="row">
                <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Nama Web
                        <div class="text-black-50 small"><b><?= $row['identitas_website'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Meta Deskripsi
                        <div class="text-black-50 small"><b><?= $row['identitas_deskripsi'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Keyword
                        <div class="text-black-50 small"><b><?= $row['identitas_keyword'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Nama Web
                        <div class="text-black-50 small"><b><?= $row['identitas_alamat'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Telepon
                        <div class="text-black-50 small"><b><?= $row['identitas_notelp'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Facebook
                        <div class="text-black-50 small"><b><?= $row['identitas_fb'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Email
                        <div class="text-black-50 small"><b><?= $row['identitas_email'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Twitter
                        <div class="text-black-50 small"><b><?= $row['identitas_tw'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Google+
                        <div class="text-black-50 small"><b><?= $row['identitas_gp'] ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Youtube
                        <div class="text-black-50 small"><b><?= $row['identitas_yb'] ?></b></div>
                    </div>
                    </div>
                </div>
                </div>
           
        </div>
      </div>
    </div>
  </div>

  <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-warning">Image</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                   
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <img style="width: 300px;" src="<?php echo base_url().'image/identitas/'.$row['identitas_favicon'];?>">
                  <br><br><br>
                  <div class="d-grid gap-2">
                      <a href="<?php echo base_url('Admin/Modul/Identitas/Identitas/change_image?id=') . $row['identitas_id']; ?>" class="btn btn-outline-warning btn-lg btn-block"> Change Image </a> 
                      <a href="#" type="button" class="btn btn-warning btn-lg btn-block" data-toggle="modal" data-target="#myModal<?php echo $row['identitas_id']; ?>">Edit</a>

                  </div>
                  </div>
                  <div class="mt-4 text-center small">
                  </div>
                </div>
              </div>
            </div>
          </div>

          
        
<!-- Content Row -->
<div class="row">

  <!-- Content Column -->
  <div class="col-lg-6 mb-4">

    


  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php endforeach; ?>

<!-- Modal Add-->
 
<!-- Modal Add-->
        
  <div class="modal fade" id="#myModal<?php echo $row['identitas_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo base_url()?>Admin/Modul/Management/Management/add"  method="post" enctype="multipart/form-data">
              <div class="form-group">
               
              <label>Username</label>
              <input type="text" name="admin_user" class="form-control"  placeholder="Id Karyawan" required>

              <label>Nama</label>
              <input type="text" name="admin_nama" class="form-control"  placeholder="Id Klaim" required>

              <label>Email</label>
              <input type="text" name="admin_email" class="form-control"  placeholder="Jumlah Diajukan" required>
                                    
              <label>Telepon</label>
              <input type="text" name="admin_telepon" class="form-control"  placeholder="Jumlah Diajukan" required>

              <label>Level Code</label>
               <select name="admin_level_kode" class="form-control">
                  <?php
                  $query_edit_level_code=$this->Management->level_code();
                  foreach ($query_edit_level_code->result_array() as $level_kode) {
                      echo "<option value=>".$level_kode['admin_level_nama']."</option>";
                  }
                  ?>      
                  </select>

                <label>Status</label>
                <select name="admin_status" id="jurusan" class="form-control">
                    <?php
                    $query_edit_status=$this->Management->admin_status();
                    foreach ($query_edit_status->result_array() as $status) { 
                    ?>
                      <option <?php echo ($status['admin_status'] == 'A') ? "selected": "" ?>>Aktif </option>
                      <option <?php echo ($status['admin_status'] == 'T') ? "selected": "" ?>>Non Aktif</option>
                    <?php break; } ?>      
                </select>

                 <label> Foto </label>  
                 <br>
                  <input type="file" name="filefoto" >
                  <br>

                <label>Keterangan</label>
                <input type="text" name="ket" class="form-control" placeholder="Keterangan" required>
              </div>
              
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>

