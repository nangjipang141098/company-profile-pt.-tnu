<!-- DataTables Example -->
<?php
                  
   $admin_user = $this->session->userdata('user');
   $query = $this->Identitas->show();
                    
    foreach ($query->result_array() as $row){
           
    ?>

      
<!-- Content Row -->

<div class="row">

<!-- Area Chart -->
<div class="col-xl-8 col-lg-7">
  <div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Nama Kelompok</h6>
      <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
        </a>
        
      </div>
    </div>
    <!-- Card Body -->
    <div class="card-body">
      <div class="chart-area">
          <!-- Color System -->
              <div class="row">
              <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Nama Web
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_website']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Meta Deskripsi
                        <div class="text-black-50 small"><b><?php cetak( $row['identitas_deskripsi']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Keyword
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_keyword']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Nama Web
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_alamat']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Telepon
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_notelp']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Facebook
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_fb']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Email
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_email'])?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Twitter
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_tw']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Google+
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_gp']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Youtube
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_yb']) ?></b></div>
                    </div>
                    </div>
                </div>
              </div>
            
              
      </div>
    </div>
  </div>
</div>


      <!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-warning">Image</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                   
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <img style="width: 300px;" src="<?php cetak( base_url().'image/identitas/'.$row['identitas_favicon'] )?>">
                  <br><br><br>
                  <div class="d-grid gap-2">
                       <a href="#" type="button" class="btn btn-warning btn-lg btn-block" data-toggle="modal" data-target="#myModal<?php cetak( $row['identitas_id'] ) ?>">Edit</a>

                  </div>
                  </div>
                  <div class="mt-4 text-center small">
                  </div>
                </div>
              </div>
            </div>
          </div>

                 
                  
                

                     <!-- Modal row-->
                      <div class="modal fade" id="myModal<?php cetak( $row['identitas_id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit Identitas</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                               


                              <form action="<?php cetak( base_url('Admin/Modul/Identitas/Identitas/edit?ni=') . $row['identitas_id'] )?>"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

                              <div class="form-group">
                                  
                                    <label>Identitas Website</label>
                                    <input type="text" name="identitas_website" class="form-control" value="<?php cetak( $row['identitas_website'] )?>" placeholder="Id Karyawan" required>

                                    <label>Deskripsi</label>
                                    <textarea class="form-control" name="identitas_deskripsi"><?php cetak( $row['identitas_deskripsi'] )?></textarea>
                                    
                                    <label>Nama</label>
                                    <textarea class="form-control" name="identitas_keyword"><?php cetak( $row['identitas_keyword'] )?></textarea>
                                   
                                    <label>Email</label>
                                    <textarea class="form-control" name="identitas_alamat"><?php cetak( $row['identitas_alamat'] ) ?></textarea>

                                    <label>Telepon</label>
                                    <input type="text" name="identitas_notelp" class="form-control" value="<?php cetak( $row['identitas_notelp'] )?>" placeholder="Telepon" required>
                                    
                                    <label>Facebook</label>
                                    <input type="text" name="identitas_fb" class="form-control" value="<?php cetak( $row['identitas_fb'] )?>" placeholder="Facebook" required>

                                    <label>E-mail</label>
                                    <input type="text" name="identitas_email" class="form-control" value="<?php cetak( $row['identitas_email'] ) ?>" placeholder="E-mail" required>

                                    <label>Twitter</label>
                                    <input type="text" name="identitas_tw" class="form-control" value="<?php cetak( $row['identitas_tw'] ) ?>" placeholder="Twitter" required>

                                    <label>Google+</label>
                                    <input type="text" name="identitas_gp" class="form-control" value="<?php cetak( $row['identitas_gp'] ) ?>" placeholder="Google+" required>

                                    <label>Youtube</label>
                                    <input type="text" name="identitas_yb" class="form-control" value="<?php cetak( $row['identitas_yb'] )?>" placeholder="Youtube" required>
                                    

                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                            
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal row-->
                  
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

<?php } ?>