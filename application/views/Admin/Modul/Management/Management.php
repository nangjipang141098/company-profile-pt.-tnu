<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Management<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>E-Mail</th>
                    <th>Phone</th>
                    <th>Foto</th>
                   
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>E-Mail</th>
                    <th>Phone</th>
                    <th>Foto</th>
                   
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Management->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['admin_user'] ) ?> </td>
                    <td><?php cetak( $row['admin_nama'] )?> </td>
                    <td><?php cetak( $row['admin_alamat'] ) ?> </td>
                    <td><?php cetak( $row['admin_email'] ) ?> </td>
                    <td><?php cetak( $row['admin_telepon'] ) ?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/management/'.$row['foto'] )?>"></td>
                    <td>
                    <a href="<?php cetak( base_url('Admin/Setting/Management/Management/delete?id=') . $row['admin_user'] ) ?>" class="btn btn-danger"> Delete </a>
           
                    <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['admin_user'] ) ?>">Edit</a>


                    </td>
                   
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['admin_user'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Management->show_where($row['admin_user']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Management/Management/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">
                                  
                                    <label>Username</label>
                                    <input type="text" name="admin_user" class="form-control" value="<?php cetak( $edit['admin_user'] )?>" placeholder="Id Karyawan" required>

                                    <label>Nama</label>
                                    <input type="text" name="admin_nama" class="form-control" value="<?php cetak( $edit['admin_nama'] )?>" placeholder="Id Klaim" required>

                                    <label>Email</label>
                                    <input type="text" name="admin_email" class="form-control" value="<?php cetak( $edit['admin_email'] )?>" placeholder="Jumlah Diajukan" required>
                                    
                                    <label>Telepon</label>
                                    <input type="text" name="admin_telepon" class="form-control" value="<?php cetak( $edit['admin_telepon'] )?>" placeholder="Jumlah Diajukan" required>

                                    

                                    <label>Level Code</label>
                                    <select name="admin_level_kode" class="form-control">
                                      <?php
                                      $query_edit_level_code=$this->Management->level_code();
                                      foreach ($query_edit_level_code->result_array() as $level_kode) {
                                        if ($edit['admin_level_kode']==$level_kode['admin_level_kode']) {
                                            $select="selected";
                                        }else{
                                            $select="";
                                        }
                                          echo "<option $select>".$level_kode['admin_level_nama']."</option>";
                                      }
                                      ?>      
                                    </select>

                                    <label>Status</label>
                                    <select name="admin_status" id="jurusan" class="form-control">
                                      <?php
                                      $query_edit_status=$this->Management->admin_status();
                                      foreach ($query_edit_status->result_array() as $status) { 
                                      ?>
                                        <option <?php cetak(($status['admin_status'] == 'A') ? "selected": "") ?>>Aktif </option>
                                        <option <?php cetak(($status['admin_status'] == 'T') ? "selected": "") ?>>Non Aktif</option>
                                     <?php break; } ?>      
                                    </select>

                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Medical Reimbust</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak (base_url())?>Admin/Modul/Management/Management/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">
               
              <label>Username</label>
              <input type="text" name="admin_user" class="form-control"  placeholder="Username" required>

              <label>Nama</label>
              <input type="text" name="admin_nama" class="form-control"  placeholder="Nama" required>

              <label>Email</label>
              <input type="text" name="admin_email" class="form-control"  placeholder="Email" required>
                                    
              <label>Telepon</label>
              <input type="text" name="admin_telepon" class="form-control"  placeholder="Telepon" required>

              <select name="kategori_id" class="form-control">
                <?php
                  $query_edit_level_code=$this->News->show_kategori();
                    foreach ($query_edit_level_code->result_array() as $level_kode) {
                      
                      echo "<option value=".$level_kode['kategori_id'].'>'.$level_kode['kategori_judul']."</option>" ;
                      }
                     ?>      
              </select>


                <label>Status</label>
                <select name="admin_status" id="jurusan" class="form-control">
                    <?php
                    $query_edit_status=$this->Management->admin_status();
                    foreach ($query_edit_status->result_array() as $status) { 
                    ?>
                      <option <?php cetak(($status['admin_status'] == 'A') ? "selected": "")  ?>>Aktif </option>
                      <option <?php cetak(($status['admin_status'] == 'T') ? "selected": "")  ?>>Non Aktif</option>
                    <?php break; } ?>      
                </select>

                 <label> Foto </label>  
                 <br>
                  <input type="file" name="filefoto" >
                  <br>

                <label>Keterangan</label>
                <input type="text" name="ket" class="form-control" placeholder="Keterangan" required>
              </div>
              
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>