<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data News<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Kategory</th>  
                    <th>Tags</th>
                    <th>Upload by</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Kategory</th>         
                    <th>Tags</th>
                    <th>Upload by</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->News->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                  <td><img style="width: 100px;" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>"></td>
                    <td><?php cetak($row['berita_judul']) ?> </td>
                    <td><?php cetak($row['kategori_id']) ?> </td>
                    <td><?php cetak($row['berita_tags']) ?> </td>
                    <td><?php cetak($row['admin_nama']) ?> </td>
                    <td>
                      <a href="<?php cetak( base_url('Admin/Modul/News/News/delete?id=') . $row['berita_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['berita_id'] )?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['berita_id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                              
                                $cek_query_edit=$this->News->show_where($row['berita_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/News/News/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">
                                  
                              <div class="form-group">
                              <label>ID</label>
                              <input type="text" name="berita_id" class="form-control" value="<?= $row['berita_id'] ?>" placeholder="ID" required readonly>

                              <div class="form-group">
                              <label>Judul</label>
                              <input type="text" name="berita_judul" class="form-control" value="<?= $row['berita_judul'] ?>" placeholder="Judul" required>

                            
                              <div class="form-group">
                              <label>Tags</label>
                              <input type="text" name="berita_tags" class="form-control" value="<?= $row['berita_tags'] ?>" placeholder="Tags" required>

                              <label>Admin Name</label>
                              <input type="text" name="mitra_link" class="form-control" value="<?=  $this->session->userdata('admin_nama'); ?>" placeholder="Upload By" required readonly>

                              <div class="form-group">
                              <label>Kategori</label>
                              <label>Level Code</label>
                                <select name="kategori_id" class="form-control">
                                  <?php
                                  $query_edit_kategori=$this->News->show_kategori();
                                  foreach ($query_edit_kategori->result_array() as $level_ketegori) {
                                    if ($row['kategori_id']==$level_ketegori['kategori_id']) {
                                      $select="selected";
                                    }else{
                                        $select="";
                                    }
                                    echo "<option $select >" .$level_ketegori['kategori_id']."</option>";
                                   
                                  }
                                  ?>   

                                    
                                  </select>
                              <label>Deskripsi</label>
                              <input type="text" name="berita_deskripsi" class="form-control" value="<?= $row['berita_deskripsi'] ?>" placeholder="Link" required>

                                  
                                    <br><br>
                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak(base_url())?>Admin/Modul/News/News/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
          
          <div class="form-group">
                                  
          <div class="form-group">
        
             <div class="form-group">
              
              
              <div class="form-group">
              <label>Judul</label>
              <input type="text" name="berita_judul" class="form-control"  placeholder="Judul" required>

          
    
              <label>Admin Name</label>
              <input type="text" name="admin_nama" class="form-control" value="<?=  $this->session->userdata('admin_nama'); ?>" placeholder="Upload By" required readonly>

              <div class="form-group">
              <label>Kategori</label>
              
              <label>Level Code</label>
              <select name="kategori_id" class="form-control">
                <?php
                  $query_edit_level_code=$this->News->show_kategori();
                    foreach ($query_edit_level_code->result_array() as $level_kode) {
                      
                      echo "<option value=".$level_kode['kategori_id'].'>'.$level_kode['kategori_judul']."</option>" ;
                      }
                     ?>      
              </select>
 
              <label>Tags</label>
                <?php
                  $query_edit_level_code=$this->News->show_tags();
                    foreach ($query_edit_level_code->result_array() as $level_tag) { ?>
                      
                      <input type="checkbox" name="berita_tags[]" value="<?php cetak($level_tag['tag_id'])  ?>" > <?= cetak($level_tag['tag_judul']) ?><br>

                      
                   <?php }  ?>      
            

              <label>Deskripsi</label>
              <input type="text" name="berita_deskripsi" class="form-control" placeholder="Link" required>

               
               
                                
              <br><br>
              <label> Foto </label>  
              <br>
              <input type="file" class="form-control" name="filefoto" >
              <br>

                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>