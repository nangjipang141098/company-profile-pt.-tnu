<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Partner<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Time</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Time</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Partner->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                     <td><?php cetak($row['mitra_nama']) ?> </td>
                    <td><?php cetak($row['mitra_link']) ?> </td>
                    <td><?php cetak($row['mitra_waktu']) ?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/partner/'.$row['mitra_gambar'])?>"></td>

                    <td>
                      <a href="<?php cetak( base_url('Admin/Modul/Partner/Partner/delete?id=') . $row['mitra_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['mitra_id'] )?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                                  
                  <!-- Modal Edit-->
                  <div class="modal fade" id="myModal<?php cetak( $row['mitra_id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                              
                                $cek_query_edit=$this->Partner->show_where($row['mitra_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Partner/Partner/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                              
                              <div class="form-group">
                              <label>Mitra ID</label>
                              <input type="text" name="mitra_id" class="form-control" value="<?= $row['mitra_id'] ?>" placeholder="Nama" required readonly>

                              <div class="form-group">
                              <label>Mitra Nama</label>
                              <input type="text" name="mitra_nama" class="form-control" value="<?= $row['mitra_nama'] ?>" placeholder="Nama" required>

                              <label>Nama</label>
                              <input type="text" name="mitra_link" class="form-control" value="<?= $row['mitra_link'] ?>" placeholder="Link" required>

                              <label>Jenis Prduk</label>
                                <select name="jenis_produk" class="form-control">
                                      <?php
                                      $query_edit_level_code=$this->Produk->show();
                                      foreach ($query_edit_level_code->result_array() as $produk) {
                                        if ($row['jenis_produk']==$produk['id_produk']) {
                                            $select="selected";
                                        }else{
                                            $select="";
                                        }
                                          echo "<option $select >" .$produk['nama_produk']."</option>";
                                      }
                                      ?>      
                                    </select>

                                    <br><br>
                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak(base_url())?>Admin/Modul/Partner/Partner/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
          
          <div class="form-group">
                                  
              <label>Mitra Nama</label>
              <input type="text" name="mitra_nama" class="form-control"  placeholder="Nama" required>

              <label>Nama</label>
              <input type="text" name="mitra_link" class="form-control" placeholder="Link" required>

                       
              <label>Jenis Prdukk</label>
                                <select name="jenis_produk" class="form-control">
                                      <?php
                                      $query_edit_level_code=$this->Produk->show();
                                      foreach ($query_edit_level_code->result_array() as $produk) {
                                        
                                          echo "<option value=".$produk['id_produk'].'>'.$produk['nama_produk']."</option>";
                                      }
                                      ?>      
                                    </select>
                              
              <br><br>
              <label> Foto </label>  
              <br>
              <input type="file" class="form-control" name="filefoto" >
              <br>
                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>