<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Produk<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Produk Name</th>
                    <th>Description (indo)</th>
                    <th>Description (eng)</th>
                    <th>Image</th>
                    <th>Action</th>         
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                     <th>Produk Name</th>
                    <th>Description (indo)</th>
                    <th>Description (eng)</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('admin_user');
                    $cek_query=$this->Produk->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['nama_produk'] ) ?> </td>
                    <td><p align="justify"><?php nl2br(cetak( $row['deskripsi_indo'] ))?></p> </td>
                    <td><?php nl2br(cetak( $row['deskripsi_ing'] )) ?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/produk/'.$row['image'] )?>"></td>
                    <td>
                    <a href="<?php cetak( base_url('Admin/Setting/Produk/Produk/delete?id=') . $row['id_produk'] ) ?>" class="btn btn-danger"> Delete </a>
                    <a href="<?php cetak( base_url('Admin/Modul/Partner/Partner/view_produk?id=') . $row['id_produk'] ) ?>" class="btn btn-danger"> View Produk <?php cetak( $row['nama_produk'] ) ?>  </a>
   
                    <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['id_produk'] ) ?>">Edit</a>

                   

                    </td>
                   
                  </tr>
                  
                 
                  
                 
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['id_produk'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Produk->show_where($row['id_produk']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Produk/Produk/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">

                                   <label>Product ID</label>
                                    <input type="text" name="id_produk" class="form-control" value="<?php cetak( $edit['id_produk'] )?>" placeholder="ID" required readonly>

                                    <label>Product Name</label>
                                    <input type="text" name="nama_produk" class="form-control" value="<?php cetak( $edit['nama_produk'] )?>" placeholder="Produk Name" required>

                                    <label>Description (Ind)</label>
                                    <textarea name="deskripsi_indo" class="form-control" placeholder="Description indonesia language" required> <?php cetak( $edit['deskripsi_indo'] )?> </textarea>

                                    <label>Description (Eng)</label>
                                    <textarea name="deskripsi_ing" class="form-control" placeholder="Description english language" required> <?php cetak( $edit['deskripsi_ing'] )?> </textarea>
                                    

                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Medical Reimbust</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak (base_url())?>Admin/Modul/Produk/Produk/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">

                                   
                                    <label>Product Name</label>
                                    <input type="text" name="nama_produk" class="form-control" placeholder="Produk Name" required>

                                    <label>Description (Ind)</label>
                                    <textarea name="deskripsi_indo" class="form-control" placeholder="Description indonesia language" required> </textarea>

                                    <label>Description (Eng)</label>
                                    <textarea name="deskripsi_ing" class="form-control" placeholder="Description english language" required> </textarea>
                                    

                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" name="filefoto" >
                                    <br>

                  <br>

          
              </div>
              
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>