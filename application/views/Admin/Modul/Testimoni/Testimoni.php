<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Testimoni<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Dari</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Dari</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                   
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Testimoni->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['dari'] )?> </td>
                    <td><?php cetak( $row['deskripsi'] )?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/testimoni/'.$row['image'] )?>"></td>

                    <td>
                      <a href="<?php cetak( base_url('Admin/Modul/Testimoni/Testimoni/delete?id=') . $row['id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['id'] ) ?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Testimoni->show_where($row['id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Testimoni/Testimoni/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">
                                  
                                    <label>Username</label>
                                    <input type="text" name="id" class="form-control" value="<?php cetak( $edit['id'] )?>" placeholder="Id Karyawan" required>

                                    <label>Nama</label>
                                    <input type="text" name="dari" class="form-control" value="<?php cetak( $edit['dari'] )?>" placeholder="Id Klaim" required>

                                    <label>Email</label>
                                    <input type="text" name="deskripsi" class="form-control" value="<?php cetak( $edit['deskripsi'] )?>" placeholder="Jumlah Diajukan" required>
                                    
                                  
                                    <br><br>
                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak(base_url())?>Admin/Modul/Testimoni/Testimoni/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">
                                  
              <label>Username</label>
              <input type="text" name="id" class="form-control"  placeholder="Id" required>

              <label>Nama</label>
              <input type="text" name="dari" class="form-control"  placeholder="Dari" required>

              <label>Deskripsi</label>
              <input type="text" name="deskripsi" class="form-control"  placeholder="Deskripsi" required>
                                  
                        
              <br><br>
              <label> Foto </label>  
              <br>
              <input type="file" class="form-control" name="filefoto" >
              <br>

                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>