<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Visitor<br>
           
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>IP Address</th>
                    <th>Access From</th>
                    <th>OS</th>
                    <th>Browser</th>
                    <th>Date Access</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>IP Address</th>
                    <th>Access From</th>
                    <th>OS</th>
                    <th>Browser</th>
                    <th>Date Access</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Visitor->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                     <td><?php cetak($row['ip_address']) ?> </td>
                    <td><?php cetak($row['dari']) ?> </td>
                    <td><?php cetak($row['os']) ?> </td>
                    <td><?php cetak($row['browser']) ?> </td>
                    <td><?php cetak($row['date']) ?> </td>
                    
                  </tr>
                  
                 
                  
           
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

