<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Menu<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode Level</th>
                    <th>Nama</th>
                    <th>Deskription</th>
                    <th>URL</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Kode Level</th>
                    <th>Nama</th>
                    <th>Deskription</th>
                    <th>URL</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Menu->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['menu_kode'] ) ?> </td>
                    <td><?php cetak( $row['menu_nama'] ) ?> </td>
                    <td><?php cetak( $row['menu_deskripsi'] ) ?> </td>
                    <td><?php cetak( $row['menu_url'] ) ?> </td>
                    <td>
                      <a href="<?php cetak( base_url('Admin/Setting/Daftar_menu/Daftar_menu/delete?id=') . $row['menu_kode'] ) ?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['menu_kode'] ) ?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak($row['menu_kode']) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Menu->show_where($row['menu_kode']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url())?>Admin/Setting/Daftar_menu/Daftar_menu/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
  
                              <div class="form-group">
                                  
                                    <label>Kode</label>
                                    <input type="text" name="menu_kode" class="form-control" value="<?php cetak( $edit['menu_kode'] )?>" placeholder="Level Kode" required >

                                    <label>Nama</label>
                                    <input type="text" name="menu_nama" class="form-control" value="<?php cetak( $edit['menu_nama'] )?>" placeholder="Nama" required>

                                    <label>Deskripsi</label>
                                    <input type="text" name="menu_deskripsi" class="form-control" value="<?php cetak( $edit['menu_deskripsi'] )?>" placeholder="Deskripsi" required>
                                    
                                    <label>URL</label>
                                    <input type="text" name="menu_url" class="form-control" value="<?php cetak( $edit['menu_url'] )?>" placeholder="URL" required>

                                    <label>Site</label>
                                    <input type="text" name="menu_site" class="form-control" value="<?php cetak( $edit['menu_site'] )?>" placeholder="Site" required>

                                    <label>Level Code</label>
                                    <select name="menu_level" class="form-control">
                                      <?php
                                      $query_edit_level_code=$this->Menu->level_code();
                                      foreach ($query_edit_level_code->result_array() as $level_kode) {
                                        if ($edit['menu_level']==$level_kode['admin_level_kode']) {
                                            $select="selected";
                                        }else{
                                            $select="";
                                        }
                                          echo "<option $select>".$level_kode['admin_level_nama']."</option>" ;
                                      }
                                      ?>      
                                    </select>

                                 
                          

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak( base_url() )?>Admin/Setting/Daftar_menu/Daftar_menu/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">
            
          
            <label>Kode</label>
            <input type="text" name="admin_level_kode" class="form-control"  placeholder="Level Kode" required>

           
            <label>Nama</label>
            <input type="text" name="menu_nama" class="form-control"  placeholder="Nama" required>

            <label>Deskripsi</label>
            <input type="text" name="menu_deskripsi" class="form-control"  placeholder="Deskripsi" required>
                                    
            <label>URL</label>
            <input type="text" name="menu_url" class="form-control"  placeholder="URL" required>

            <label>Site</label>
            <input type="text" name="menu_site" class="form-control"  placeholder="Site" required>

            <label>Level Code</label>
              <select name="menu_level" class="form-control">
                <?php
                  $query_edit_level_code=$this->Menu->level_code();
                    foreach ($query_edit_level_code->result_array() as $level_kode) {
                      
                      echo "<option $select>".$level_kode['admin_level_nama']."</option>" ;
                      }
                     ?>      
              </select>
   
                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>