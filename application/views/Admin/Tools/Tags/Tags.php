<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Tags<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Judul</th>
                    <th>SEO</th>
                    <th>Aktion</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Judul</th>
                    <th>SEO</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Tags->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['tag_id'] )?> </td>
                    <td><?php cetak( $row['tag_judul'] )?> </td>
                    <td><?php cetak( $row['tag_seo'] )?> </td>
                    <td>
                      <a href="<?php cetak( base_url('Admin/Tools/Tags/Tags/delete?id=') . $row['tag_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['tag_id'] )?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['tag_id'] ) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Tags->show_where($row['tag_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url())?>Admin/Tools/Tags/Tags/edit"  method="post" enctype="multipart/form-data">
                                  <div class="form-group">
                                  
                                    <label>ID</label>
                                    <input type="text" name="tag_id" class="form-control" value="<?php cetak( $edit['tag_id'] )?>" placeholder="Id" required>

                                    <label>Judul</label>
                                    <input type="text" name="tag_judul" class="form-control" value="<?php cetak( $edit['tag_judul'] )?>" placeholder="Judul" required>

                                    <label>SEO</label>
                                    <input type="text" name="tag_seo" class="form-control" value="<?php cetak( $edit['tag_seo'] )?>" placeholder="SEO" required>
                                    
                          

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak( base_url())?>Admin/Tools/Tags/Tags/add"  method="post" enctype="multipart/form-data">
          <div class="form-group">
                                  
              <label>ID</label>
              <input type="text" name="tag_id" class="form-control"  placeholder="Id" required>

              <label>Judul</label>
              <input type="text" name="tag_judul" class="form-control"  placeholder="Jusul" required>

              <label>SEO</label>
              <input type="text" name="tag_seo" class="form-control"  placeholder="SEO" required>
                                  
           

                          
                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>