

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container">

        

        <div class="row">
          <div class="col-lg-4 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <br><br><h4 data-aos="fade-up"><b>Video Company</b></h4>
              <div class="col-lg-4 col-lg-8 video-box d-flex justify-content-center align-items-stretch" data-aos="fade-right">
                <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
              </div>
            </div>

          <div class="col-lg-1 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <br><br><h4 data-aos="fade-up"><b><?= $about_us ?></b></h4>
            <h3 data-aos="fade-up"><?= $title_about_us ?></h3>
            <p data-aos="fade-up"><?= $description_about_us ?></p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->
