
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up"><?= $contact ?></h2>
        </div>

        <div class="row justify-content-center">

        <?php foreach($this->Identitas->show()->result_array() as $row){ ?>
          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up">
            <div class="info-box">
              <i class="bx bx-map"></i>
              <h3>Our Address</h3>
              <p><?php cetak($row['identitas_alamat'])?></p>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="100">
            <div class="info-box">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
              <p><?php cetak($row['identitas_email'])?></p>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="info-box">
              <i class="bx bx-phone-call"></i>
              <h3>Call Us</h3>
              <p><?php cetak($row['identitas_notelp'])?></p>
            </div>
          </div>

          <?php } ?>
        </div>

        

      </div>
    </section><!-- End Contact Section -->