
  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          

        </div>
      </div>
    </div>

    <div class="container d-lg-flex py-4">

      <div class="mr-lg-auto text-center text-lg-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Flexor</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ -->
          Designed by <a href="<?= base_url() ?>https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
      <?php foreach ($this->Identitas->show()->result_array as $row){ ?>
      <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
        <a href="<?= base_url() ?>#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="<?= base_url() ?><?php cetak($row['identitas_fb']) ?>" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="<?= base_url() ?>#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="<?= base_url() ?>#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="<?= base_url() ?>#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
      <?php } ?>
    </div>
  </footer><!-- End Footer -->