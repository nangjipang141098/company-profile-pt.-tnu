
    <!-- ======= Team Section ======= -->
    <section id="galery" class="team section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up"><?= $Galery ?></h2>
        </div>

        <div class="row">
          <?php foreach($this->Fasilitas->show_limit()->result_array() as $row){ ?>
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
              <div class="member">
                <div class="member-img">
                  <img src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" class="img-fluid" alt="">
                  <!-- <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                  </div> -->
                </div>
                <div class="portfolio-info">
                
                        <p><?php cetak($row['fasilitas_nama']) ?> <br> <?php cetak($row['fasilitas_deskripsi']) ?></p>
                        <a href="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3">Zoom<i class="bx bx-plus"></i></a>
                </div>
              </div>  
            </div>
          <?php } ?>
        </div>

        <?php if($this->Fasilitas->show_limit()->num_rows() >=8 ){ ?>
         <div class="text-center"><a href="<?= base_url() ?>Dashboard/Dashboard/galery_page">Show All</a></div>   
        <?php } ?>

      </div>
    </section><!-- End Team Section -->