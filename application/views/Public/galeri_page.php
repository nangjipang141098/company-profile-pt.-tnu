<!DOCTYPE html>
<html lang="en">
<?php require_once('head.php') ?>
<body>

  

 

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
   
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="<?= base_url() ?>Welcome"> Home</a></li>
          <li>Inner Page</li>
        </ol>
        <h2>Galery Page</h2>
      </div>
    </section><!-- End Breadcrumbs -->

    <section class="inner-page pt-3">
      <div class="container">
        <p>
        <div class="container">

                <div class="section-title">
                <h2 data-aos="fade-up"><?= $Galery ?></h2>
                </div>

                <div class="row">

                <?php foreach($this->Fasilitas->show()->result_array() as $row){ ?>
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                    <div class="member">
                    <div class="member-img">
                        <img src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" class="img-fluid" alt="">
                        <!-- <div class="social">
                        <a href=""><i class="icofont-twitter"></i></a>
                        <a href=""><i class="icofont-facebook"></i></a>
                        <a href=""><i class="icofont-instagram"></i></a>
                        <a href=""><i class="icofont-linkedin"></i></a>
                        </div> -->
                    </div>
                    <div class="portfolio-info">
                    
                        <p><?php cetak($row['fasilitas_nama']) ?> <br> <?php cetak($row['fasilitas_deskripsi']) ?></p>
                        <a href="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3">Zoom<i class="bx bx-plus"></i></a>
                      
                    </div>
                    </div>
                </div>
                <?php } ?>

                



                </div>

                </div>
        </p>
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php require_once('footer.php'); ?>
 <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <?php require_once('vendor_js_files.php'); ?>

</body>

</html>