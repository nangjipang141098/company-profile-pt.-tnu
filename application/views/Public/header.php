
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="index.html"><span><?= $title ?></span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.html"><?= $home ?></a></li>
         
      
         
          <li class="drop-down"><a href="<?= base_url() ?>#"><?= $Drop_down_corporate ?></a>
            <ul>
                 <li><a href="#about"><?= $About ?></a></li>
              
            </ul>
          </li>
          <li class="drop-down"><a href="<?= base_url() ?>#"><?= $Drop_down_article_media ?></a>
            <ul>
                <li><a href="#galery"><?= $Galery ?></a></li>
                <li><a href="#faq"><?= $Drop_down_article_media ?></a></li>
              
            </ul>
          </li>
          <li class="drop-down"><a href="<?= base_url() ?>#"><?= $Drop_down_pns ?></a>
            <ul>
            
               <li><a href="#services"><?= $Services ?></a></li>
               <li><a href="#product"><?= $Portfolio ?></a></li>
               <li><a href="#req_documentation"><?= $request_documentation ?></a></li>
               <li><a href="#testimonials"><?= $Testimoni ?></a></li>
            </ul>
          </li>
          <li class="drop-down"><a href="<?= base_url() ?>#"><?= $Drop_down ?></a>
            <ul>
              <li><a href="<?= base_url() ?>Welcome?id=indo">Indonesia</a></li>
              <li><a href="<?= base_url() ?>Welcome?id=english">English</a></li>
            </ul>
          </li>
          <li><a href="#contact"><?= $contact ?></a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->