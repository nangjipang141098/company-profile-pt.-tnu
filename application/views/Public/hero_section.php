 <!-- ======= Hero Section ======= -->
 <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="container" data-aos="fade-in">
      <h1><?= $wellcome ?></h1>
      <h2> <p class="description"><?= $wellcome2 ?></p></h2>
      <div class="d-flex align-items-center">
        <i class="bx bxs-right-arrow-alt get-started-icon"></i>
      </div>
    </div>
  </section><!-- End Hero -->
