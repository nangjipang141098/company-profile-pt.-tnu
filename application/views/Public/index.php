<!DOCTYPE html>
<html lang="en">
  <?php require_once('head.php');?>

<body>

  <?php require_once('header.php');?>
  <?php require_once('hero_section.php'); ?>

  <main id="main">

      <?php require_once('about_section.php') ?>

      <?php //require_once('visi_misi.php') ?>

      <?php require_once('service_section.php') ?>

      <?php // require_once('portfolio_section.php') ?>

      <?php require_once('product_section.php') ?>

      <?php require_once('request_documentation.php') ?>

      <?php require_once('testimonial_section.php') ?>
      
      <?php require_once('galeri.php') ?>

      <?php //require_once('team_section.php') ?>
      
      <?php require_once('latest_update_section.php') ?>
      
      <?php require_once('jobs.php') ?>

      <?php cetak (require_once('visitor.php')) ?>


      <?php require_once('contact.php') ?>
  </main><!-- End #main -->

  
  <?php require_once('footer.php') ?>

  <a href="<?= base_url() ?>#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <?php require_once('vendor_js_files.php') ?>
</body>

</html>