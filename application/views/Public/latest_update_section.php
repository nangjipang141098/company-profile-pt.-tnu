

    <!-- ======= LATEST UPDATES Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up"><?= $latest_update_section ?></h2>
        </div>

        <div class="faq-list">
          <ul>
            <?php foreach($this->News->show_where_news(0, 1)->result_array() as $row){?>
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-2" class="collapsed"><?php cetak( $row['berita_judul']) ?> <br> <?php cetak ($row['admin_nama']." || ".$row['berita_waktu']) ?>  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                <p>
                <img style="width: 600px;" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>"><br>
                <?php cetak( $row['berita_deskripsi']) ?>                  
                 </p>
              </div>
            </li>
            <?php } ?>
            
            <?php foreach($this->News->show_where_news(1, 1)->result_array() as $row){?>
            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3" class="collapsed"><?php cetak( $row['berita_judul']) ?> <br> <?php cetak ($row['admin_nama']." || ".$row['berita_waktu']) ?>  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                <p>
                <img style="width: 600px;" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>"><br>
                <?php cetak( $row['berita_deskripsi']) ?>             
                      </p>
              </div>
            </li>
            <?php } ?>

            <?php foreach($this->News->show_where_news(2, 1)->result_array() as $row){?>
            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-4" class="collapsed"><?php cetak( $row['berita_judul']) ?> <br> <?php cetak ($row['admin_nama']." || ".$row['berita_waktu']) ?>  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                <p><img style="width: 600px;" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>"><br>
                <?php cetak( $row['berita_deskripsi']) ?>                   </p>
              </div>
            </li>
             <?php } ?>

             <?php foreach($this->News->show_where_news(3, 1)->result_array() as $row){?>
            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-5" class="collapsed"><?php cetak( $row['berita_judul']) ?> <br> <?php cetak ($row['admin_nama']." || ".$row['berita_waktu']) ?> <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                <p><img style="width: 600px;" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>"><br>
                <?php cetak( $row['berita_deskripsi']) ?>                   </p>
              </div>
            </li>
            <?php } ?>

          </ul>
        </div>

      </div>
    </section><!-- End LATEST UPDATES Section -->
