
 
 <!-- ======= Portfolio Section ======= -->
 <section id="product" class="portfolio section-bg">
        <div class="section-title" data-aos="fade-up">
          <h2><?= $Portfolio ?></h2>
          <p> </p>
        </div>
      <div class="container">

      <div class="owl-carousel testimonials-carousel">
          
          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA E-TILA</b></h5>
              <p>     
              <h6>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_etila) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA E-RISK</b></h5>
            <p>
              <h6>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_erisk) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA WBS</b></h5>
            <p>
              <h6>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_wbs) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA KMS</b></h5>
            <p>     
              <h6><i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_kms) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i></h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA E-OFFICE</b></h5>
            <p>
              <h6>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_eoffice) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA KPPU</b></h5>
            <p>
              <h6>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_kppu) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </h6></a>
            </p>
          </div>

          <div class="testimonial-item">
          <a class="btn btn-warning btn-lg btn-block">
            <h5><b>BANGGA UPG</b></h5>
            <p>     
              <h6><i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak($description_upg) ?>
              <i class="bx bxs-quote-alt-right quote-icon-right"></i></h6></a>
            </p>
          </div>

        </div>
   
   
        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter=".bangga-etila" >BANGGA E-TILA</li>
              <li data-filter=".bangga-erisk">BANGGA E-RISK</li>
              <li data-filter=".bangga-wbs">BANGGA WBS</li>
              <li data-filter=".bangga-kms">BANGGA KMS</li>
              <li data-filter=".bangga-eoffice">BANGGA E-OFFICE</li>
              <li data-filter=".bangga-kppu">BANGGA KPPU</li>
              <li data-filter=".bangga-upg">BANGGA UPG</li>
            </ul>
          </div>
        </div>
       
        <div class="row portfolio-container">
        <!-- bangga etila --> 
        <?php foreach($this->Partner->show_where_id(1)->result_array() as $etila): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-etila">
          <img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4><?php cetak($etila['mitra_nama']) ?></h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>

          <!-- bangga e-risk -->
          <?php foreach($this->Partner->show_where_id(2)->result_array() as $erisk): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-erisk">
          <img src="<?php cetak( base_url().'image/partner/'.$erisk['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>
          
          <!-- bangga wbs -->
          <?php foreach($this->Partner->show_where_id(3)->result_array() as $wbs): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-wbs">
          <img src="<?php cetak( base_url().'image/partner/'.$wbs['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>

           <!-- bangga kms -->
          <?php foreach($this->Partner->show_where_id(4)->result_array() as $kms): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-kms">
          <img src="<?php cetak( base_url().'image/partner/'.$kms['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>

          <!-- bangga eoffice -->
          <?php foreach($this->Partner->show_where_id(5)->result_array() as $eoffice): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-eoffice">
          <img src="<?php cetak( base_url().'image/partner/'.$eoffice['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>

          <!-- bangga KPPU -->
          <?php foreach($this->Partner->show_where_id(6)->result_array() as $kppu): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-eoffice">
          <img src="<?php cetak( base_url().'image/partner/'.$kppu['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>

          <!-- bangga UPG -->
          <?php foreach($this->Partner->show_where_id(7)->result_array() as $upg): ?>
          <div class="col-lg-4 col-md-6 portfolio-item bangga-eoffice">
          <img src="<?php cetak( base_url().'image/partner/'.$upg['mitra_gambar'])?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="<?= base_url() ?>template/template_public/assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endforeach; ?>
         

        


        </div>

  

      </div>
    </section><!-- End Portfolio Section -->