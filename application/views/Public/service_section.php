
      <section id="services" class="services ">
      <div class="section-title" data-aos="fade-up">
          <h2><?= $services ?></h2>
          <p> </p>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
            <div class="icon-box">
              <div class="icon"><i class="icofont-computer"></i></div>
              <h4 class="title"><a href=""><?= $app_development ?></a></h4>
              <p class="description"><?= $type_app_develompemnt ?></p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="icofont-chart-bar-graph"></i></div>
              <h4 class="title"><a href=""><?= $infrastructure_support ?></a></h4>
              <p class="description"><?= $type_infrastructure_support ?></p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="icofont-earth"></i></div>
              <h4 class="title"><a href=""><?= $staffing_service_models ?></a></h4>
              <p class="description"><?= $type_staffing_service_models ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    