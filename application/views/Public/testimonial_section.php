
    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="fade-up">

        <div class="owl-carousel testimonials-carousel">
         <?php foreach($this->Testimoni->show()->result_array() as $row){ ?>
          <div class="testimonial-item">
            <img src="<?php cetak( base_url().'image/testimoni/'.$row['image'] )?>" class="testimonial-img" alt="">
            <h3><?php cetak( $row['dari'] )?> </h3>
           
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              <?php cetak( $row['deskripsi'] )?>        
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>
         <?php } ?>

        

        </div>

      </div>
    </section><!-- End Testimonials Section -->
