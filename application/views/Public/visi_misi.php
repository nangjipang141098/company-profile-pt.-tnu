
    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
      <div class="container">

        
      <div class="section-title" data-aos="fade-up">
          <h2><?= $title_vision_mision ?></h2>
          <p> </p>
        </div>

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card" style="background-image: url(<?= base_url() ?>template/template_public/assets/img/values-1.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href=""><?= $title_vision ?></a></h5>
                <p class="card-text"><?= $description_vision ?></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="100">
            <div class="card" style="background-image: url(<?= base_url() ?>template/template_public/assets/img/values-2.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href=""><?= $title_mision ?></a></h5>
                <p class="card-text"><?= $description_mision ?></p>
              </div>
            </div>
          </div>

      </div>
    </section><!-- End Values Section -->
