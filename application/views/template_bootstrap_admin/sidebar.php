 <!-- Sidebar -->
 <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Modul</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Select</h6>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Modul/Identitas/Identitas">Identitas</a>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Modul/Produk/Produk">Produk & Service</a>
          <a class="dropdown-item" href="<?php cetak( base_url() )  ?>Admin/Modul/Fasilitas/Fasilitas">Fasilitas</a>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Modul/Testimoni/Testimoni">Testimoni</a> 
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Modul/News/News">News</a> 
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Modul/Visitor/Visitor">Visitor</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Tools</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown2">
          <h6 class="dropdown-header">Select</h6>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Tools/Tags/Tags/">Tags</a>
          <a class="dropdown-item" href="#">Download File</a>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Tools/Galeri_video/Galeri_video">Galery video</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Setting</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown2">
          <h6 class="dropdown-header">Daftar Menu</h6>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Setting/Daftar_akses/Daftar_akses">List Access</a>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Setting/Daftar_menu/Daftar_menu">List Menu</a>
          <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Setting/Management/Management">User</a>
          <!-- <a class="dropdown-item" href="<?php cetak( base_url() ) ?>Admin/Setting/Hak_akses/Hak_akses">Hak Akses</a> -->
        </div>
      </li>
    
    </ul>