-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 08, 2021 at 06:20 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banggaso_nindya`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_user` varchar(25) NOT NULL,
  `admin_pass` varchar(50) NOT NULL,
  `admin_nama` varchar(30) NOT NULL,
  `admin_alamat` varchar(250) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_telepon` varchar(15) NOT NULL,
  `admin_ip` varchar(12) DEFAULT NULL,
  `admin_online` int(10) DEFAULT NULL,
  `admin_level_kode` int(3) NOT NULL,
  `admin_status` char(1) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_user`, `admin_pass`, `admin_nama`, `admin_alamat`, `admin_email`, `admin_telepon`, `admin_ip`, `admin_online`, `admin_level_kode`, `admin_status`, `foto`) VALUES
('qqq', '', 'qqq', '', 'qqq', 'qqq', NULL, NULL, 0, 'N', '6ca8570d342e7247d23e07f12df55c74.png'),
('reza', 'reza', 'rezaa', 'dd', 'ddd', '1122', NULL, NULL, 1, 'A', 'b7e39bd491fa2e68f336d24b6aa33b9f.png'),
('s', '', 's', '', 's', 's', NULL, NULL, 0, 'A', '8a715dffb61acf95cc7c68dc6877b000.jpg'),
('www', '', 'www', '', 'wwww', '1122', NULL, NULL, 0, 'A', '952c93f546a7d9e0320048e81c909e83.png'),
('wwwq', '', 'www', '', 'ww', 'ff', NULL, NULL, 0, 'A', '8cbb529a82968969fae2135734d8ebc8.png'),
('wwwqww', '', 'www', '', 'wwww', 'w', NULL, NULL, 2, 'A', 'a00088f166c9f63233e44ab63fcbc8c9.png');

-- --------------------------------------------------------

--
-- Table structure for table `admin_level`
--

CREATE TABLE `admin_level` (
  `admin_level_kode` int(3) NOT NULL,
  `admin_level_nama` varchar(30) NOT NULL,
  `admin_level_status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_level`
--

INSERT INTO `admin_level` (`admin_level_kode`, `admin_level_nama`, `admin_level_status`) VALUES
(1, 'Administrator', 'A'),
(2, 'Operator', 'A'),
(5, 'Member', 'A'),
(6, 'programmers', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `agenda_id` int(11) NOT NULL,
  `agenda_tema` varchar(100) NOT NULL,
  `agenda_deskripsi` text NOT NULL,
  `agenda_mulai` date NOT NULL,
  `agenda_selesai` date NOT NULL,
  `agenda_tempat` varchar(100) NOT NULL,
  `agenda_jam` varchar(50) NOT NULL,
  `agenda_gambar` varchar(100) DEFAULT NULL,
  `agenda_posting` datetime NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(3) NOT NULL,
  `album_judul` varchar(100) NOT NULL,
  `album_deskripsi` text NOT NULL,
  `album_gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_judul`, `album_deskripsi`, `album_gambar`) VALUES
(29, 'Album 1 Politeknik Pos Indonesia', '<p>\r\n	Album 1 Politeknik Pos Indonesia</p>\r\n', '1441113457-POLPOS.png'),
(30, 'Album 2 Politeknik Pos Indonesia', '<p>\r\n	Album 2 Politeknik Pos Indonesia</p>\r\n', '1441113487-slide1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `album_galeri`
--

CREATE TABLE `album_galeri` (
  `galeri_id` int(5) NOT NULL,
  `galeri_judul` varchar(100) NOT NULL,
  `galeri_deskripsi` varchar(250) NOT NULL,
  `galeri_gambar` varchar(100) NOT NULL,
  `galeri_waktu` datetime NOT NULL,
  `album_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_galeri`
--

INSERT INTO `album_galeri` (`galeri_id`, `galeri_judul`, `galeri_deskripsi`, `galeri_gambar`, `galeri_waktu`, `album_id`) VALUES
(45, 'Contoh Gallery2', '<p>\r\n	Contoh Gallery2</p>\r\n', '1440333261-slide2.JPG', '2015-08-23 19:34:21', 29),
(46, 'Contoh Gallery4', '<p>\r\n	Contoh Gallery4</p>\r\n', '1440413846-logo poltekpos.png', '2015-08-24 17:56:47', 29),
(47, 'Contoh Gallery1', '<p>\r\n	Contoh Gallery1</p>\r\n', '1440413903-pol1.PNG', '2015-08-24 17:58:23', 29),
(48, 'Contoh Gallery3', '<p>\r\n	Contoh Albums</p>\r\n', '1440413943-pol2.PNG', '2015-08-24 17:59:03', 29),
(49, 'Contoh Gallery5', '<p>\r\n	Contoh Gallery5</p>\r\n', '1440413987-pol3.PNG', '2015-08-24 17:59:47', 29),
(50, 'Contoh Gallery6', '<p>\r\n	Contoh Gallery6</p>\r\n', '1440414020-DESAIN KEMEJA.png', '2015-08-24 18:00:20', 29),
(51, 'Contoh Gallery7', '<p>\r\n	Contoh Gallery7</p>\r\n', '1440414048-slide2.JPG', '2015-08-24 18:00:48', 29),
(52, 'Contoh Gallery8', '<p>\r\n	Contoh Gallery8</p>\r\n', '1440414112-SEMINAR.jpg', '2015-08-24 18:01:52', 29),
(53, 'Album 2 Politeknik Pos Indonesia', '<p>\r\n	Album 2 Politeknik Pos Indonesia</p>\r\n', '1441113588-launching.jpg', '2015-09-01 20:19:48', 30);

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `berita_id` int(5) NOT NULL,
  `berita_judul` varchar(100) NOT NULL,
  `headline` enum('N','Y') DEFAULT 'N',
  `berita_deskripsi` text NOT NULL,
  `berita_waktu` datetime NOT NULL,
  `berita_gambar` varchar(100) NOT NULL,
  `berita_hits` int(3) DEFAULT NULL,
  `berita_tags` varchar(100) NOT NULL,
  `kategori_id` int(3) NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`berita_id`, `berita_judul`, `headline`, `berita_deskripsi`, `berita_waktu`, `berita_gambar`, `berita_hits`, `berita_tags`, `kategori_id`, `admin_nama`) VALUES
(11, 'DDaniel Zhang Gantikan Jack Ma Pimpin Alibaba, Ini Profilnya', NULL, 'ddd', '2021-01-06 22:44:12', '4c9eac2b1c45de53f7ade00ebff01765.png', NULL, '3 5', 4, 'rezaa');

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(2525, 1609140828, '103.233.88.224', '1450'),
(2526, 1609140835, '103.233.88.224', '4267'),
(2527, 1609143988, '103.233.88.224', '0345'),
(2528, 1609144027, '103.233.88.224', '6947'),
(2529, 1609144057, '103.233.88.224', '8513');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `download_id` int(5) NOT NULL,
  `download_judul` varchar(50) NOT NULL,
  `download_deskripsi` varchar(250) NOT NULL,
  `download_file` varchar(250) NOT NULL,
  `download_hits` int(5) NOT NULL,
  `download_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `fasilitas_id` int(5) NOT NULL,
  `fasilitas_nama` varchar(255) NOT NULL,
  `fasilitas_deskripsi` text NOT NULL,
  `fasilitas_gambar` varchar(255) NOT NULL,
  `fasilitas_waktu` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `galeri_video`
--

CREATE TABLE `galeri_video` (
  `video_id` int(11) NOT NULL,
  `video_judul` varchar(100) NOT NULL,
  `video_deskripsi` varchar(250) NOT NULL,
  `video_link` varchar(200) NOT NULL,
  `video_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri_video`
--

INSERT INTO `galeri_video` (`video_id`, `video_judul`, `video_deskripsi`, `video_link`, `video_waktu`) VALUES
(1, 'test', 'eee', 'eee', '2021-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `hak_akses_id` int(3) NOT NULL,
  `deskripsi` varchar(70) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `level_menu` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`hak_akses_id`, `deskripsi`, `menu`, `level_menu`) VALUES
(1, 'bagusss', 'Administrator', 1),
(2, 'bagusSSSS', 'Operator', 2),
(1, 'bagus', 'Website', 2),
(2, 'bagus', 'vission and mission', 1);

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `identitas_id` int(3) NOT NULL,
  `identitas_website` varchar(250) NOT NULL,
  `identitas_deskripsi` text NOT NULL,
  `identitas_keyword` text NOT NULL,
  `identitas_alamat` varchar(250) NOT NULL,
  `identitas_notelp` char(70) NOT NULL,
  `identitas_fb` varchar(100) NOT NULL,
  `identitas_email` varchar(100) NOT NULL,
  `identitas_tw` varchar(100) NOT NULL,
  `identitas_gp` varchar(100) NOT NULL,
  `identitas_yb` varchar(100) NOT NULL,
  `identitas_favicon` varchar(250) NOT NULL,
  `identitas_author` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`identitas_id`, `identitas_website`, `identitas_deskripsi`, `identitas_keyword`, `identitas_alamat`, `identitas_notelp`, `identitas_fb`, `identitas_email`, `identitas_tw`, `identitas_gp`, `identitas_yb`, `identitas_favicon`, `identitas_author`) VALUES
(1, 'BANGGA SOLUTIONS', 'PT. TRI NINDYA UTAMA atau yang lebih dikenal dengan BANGGA SOLUTIONS merupakan perusahaan konsultan teknologi informasi dan jaringan yang berdiri di Jakarta tahun 2012. PT. TRI NINDYA UTAMA menyediakan berbagai macam solusi Teknologi Informasi (TI) dan bisnis proses Termasuk didalamnya pengembangan aplikasi terkini, penerapan infrastruktur TI, pengembangan dan perawatan Website perusahaan penyedia layanan staf TI yang professional di bidangnya.', 'PT. TRI NINDYA UTAMA atau yang lebih dikenal dengan BANGGA SOLUTIONS merupakan perusahaan konsultan teknologi informasi dan jaringan yang berdiri di Jakarta tahun 2012. PT. TRI NINDYA UTAMA menyediakan berbagai macam solusi Teknologi Informasi (TI) dan bisnis proses Termasuk didalamnya pengembangan aplikasi terkini, penerapan infrastruktur TI, pengembangan dan perawatan Website perusahaan penyedia layanan staf TI yang professional di bidangnya.', 'Jl. H. Saaba Raya Kav F1-F2, Meruya Selatan, Kembangan, Jakarta Barat, 11650', 'Phone :(+62)21-5842138', 'https://www.facebook.com/trinindyautama', 'trinindyautama@gmail.com', '-', 'https://plus.google.com/112086280882257752442', '-', 'bs.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `join_event`
--

CREATE TABLE `join_event` (
  `join_id` int(3) NOT NULL,
  `join_nama` varchar(30) NOT NULL,
  `agenda_id` int(3) NOT NULL DEFAULT '0',
  `join_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_event`
--

INSERT INTO `join_event` (`join_id`, `join_nama`, `agenda_id`, `join_waktu`) VALUES
(13, 'asrul', 12, '2015-09-25 19:33:15'),
(16, 'Jaka', 12, '2015-09-25 21:30:27'),
(17, 'Nava Gia Ginasta', 12, '2015-09-25 22:28:23');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(3) NOT NULL,
  `kategori_judul` varchar(50) NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_judul`, `admin_nama`) VALUES
(3, 'Berita', 'Nava Gia Ginasta'),
(4, 'Jobs', 'Nava Gia Ginasta'),
(5, 'Networking', 'Arief Cholil Ampri'),
(6, 'Teknologi', 'Arief Cholil Ampri'),
(7, 'Games', 'Arief Cholil Ampri'),
(8, 'Digital', 'Arief Cholil Ampri'),
(9, 'Fotografi', 'Arief Cholil Ampri'),
(10, 'Mobile', 'Arief Cholil Ampri'),
(11, 'Android', 'Arief Cholil Ampri');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `komentar_id` int(11) NOT NULL,
  `komentar_nama` varchar(30) NOT NULL,
  `komentar_deskripsi` text NOT NULL,
  `komentar_waktu` datetime NOT NULL,
  `berita_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `management_id` int(5) NOT NULL,
  `management_nama` varchar(100) NOT NULL,
  `management_jabatan` varchar(100) NOT NULL,
  `management_team` varchar(250) NOT NULL,
  `management_deskripsi` text NOT NULL,
  `management_email` varchar(100) NOT NULL,
  `management_fb` varchar(250) NOT NULL,
  `management_twitter` varchar(250) NOT NULL,
  `management_gp` varchar(250) NOT NULL,
  `management_foto` varchar(250) NOT NULL,
  `management_post` datetime NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_kode` int(11) NOT NULL,
  `menu_nama` varchar(50) NOT NULL,
  `menu_deskripsi` varchar(50) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_site` enum('A','H') NOT NULL DEFAULT 'A',
  `menu_level` char(1) NOT NULL,
  `menu_subkode` int(11) NOT NULL,
  `menu_urutan` int(2) NOT NULL,
  `menu_status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_kode`, `menu_nama`, `menu_deskripsi`, `menu_url`, `menu_site`, `menu_level`, `menu_subkode`, `menu_urutan`, `menu_status`) VALUES
(1, 'Berandaa', 'Beranda', 'admin', 'A', '1', 0, 1, 'A'),
(3, 'Website', 'Website', 'website', 'A', '1', 0, 2, 'A'),
(7, 'Pengaturan', 'Pengaturan', 'pengaturan', 'A', '1', 0, 7, 'A'),
(9, 'Profil', 'Informasi Profil', '#', 'A', '2', 1, 1, 'H'),
(11, 'Daftar Menu', 'Informasi Daftar Menu', 'pengaturan/menu', 'A', '3', 13, 1, 'A'),
(12, 'Pengaturan Umum', 'Informasi Pengaturan Umum', 'pengaturan/umum', 'A', '2', 7, 1, 'H'),
(13, 'Setting', 'fa fa-cog fa-fw', '#', 'A', '2', 1, 4, 'A'),
(16, 'Daftar Pengguna', 'Daftar Pengguna', 'pengaturan/pengguna/view', 'A', '3', 13, 1, 'A'),
(17, 'Tambah Pengguna', 'Tambah Pengguna', 'pengaturan/pengguna/tambah', 'A', '3', 13, 2, 'H'),
(19, 'Hak Akses Kelompok', 'Hak Akses Kelompok', 'pengaturan/hak_akses', 'A', '3', 13, 4, 'A'),
(79, 'Menu Utama', 'fa-list', '#', 'A', '2', 1, 3, 'A'),
(80, 'MODUL WEB', 'Informasi Website', '#', 'A', '2', 3, 3, 'H'),
(84, 'Agenda / Events', 'Informasi Agenda / Events', 'website/agenda', 'A', '3', 79, 5, 'A'),
(93, 'Public', 'Menu Public', '#', 'A', '1', 0, 0, 'A'),
(94, 'BERANDA', 'Beranda', 'home', 'A', '2', 93, 1, 'H'),
(97, 'Kategori Berita', 'Informasi Kategori Berita', 'website/kategori', 'A', '3', 79, 1, 'H'),
(98, 'Berita', 'Informasi Berita', 'website/berita', 'A', '3', 79, 2, 'A'),
(99, 'Tags', 'Informasi Tags', 'website/tags', 'A', '3', 79, 3, 'A'),
(100, 'Komentar', 'Informasi Komentar', 'website/komentar', 'A', '3', 79, 4, 'A'),
(101, 'Download File', 'Informasi Download File', 'website/download_file', 'A', '3', 79, 6, 'A'),
(104, 'Galeri Video', 'Informasi Galeri Video', 'website/galeri_video', 'A', '3', 79, 9, 'A'),
(105, 'Modul Website', 'fa-laptop', '#', 'A', '2', 1, 2, 'A'),
(106, 'Identitas Website', 'Informasi Identitas Website', 'website/identitas/edit/1', 'A', '3', 105, 1, 'A'),
(157, 'Partners / Mitra Kerja', 'Informasi Partner / Mitra Kerja', 'website/mitra_kerja', 'A', '3', 79, 10, 'A'),
(158, 'Kelompok Pengguna', 'Kelompok Pengguna', 'pengaturan/kelompok_pengguna', 'A', '3', 13, 3, 'A'),
(160, 'Home', ' fa-home', 'admin', 'A', '2', 1, 1, 'A'),
(161, 'Dashboard', 'Dashboard', 'admin', 'A', '3', 160, 1, 'A'),
(162, 'Management Team', 'Informasi Management Team', 'website/management', 'A', '3', 105, 2, 'A'),
(163, 'Home', '#', '#tf-home', 'A', '2', 93, 1, 'A'),
(164, 'Service', '#', '#tf-services', 'A', '2', 93, 2, 'A'),
(165, 'Companies', '#', '#tf-companies', 'A', '2', 93, 3, 'A'),
(166, 'Galeri Foto', '#', '#gl-foto', 'A', '2', 93, 4, 'A'),
(167, 'About Us', '#', '#tf-about', 'A', '2', 93, 5, 'A'),
(168, 'Updates', '#', '#tf-updates', 'A', '2', 93, 6, 'A'),
(169, 'Contact', '#', '#tf-contact', 'A', '2', 93, 7, 'A'),
(170, 'News', '#', 'news', 'A', '3', 168, 1, 'A'),
(171, 'Events', '#', 'events', 'A', '3', 168, 2, 'A'),
(172, 'Jobs', '#', 'jobs', 'A', '3', 168, 3, 'A'),
(173, 'Fasilitas', 'Informasi Fasilitas', 'website/fasilitas', '', '3', 105, 3, 'A'),
(174, 'PRODUCT', 'Product', 'products', 'A', '1', 0, 3, 'A'),
(175, 'vission and mission', '#', 'visi', 'A', '1', 0, 5, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `menu_admin`
--

CREATE TABLE `menu_admin` (
  `menu_admin_kode` int(11) NOT NULL,
  `menu_kode` int(11) NOT NULL,
  `admin_level_kode` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_admin`
--

INSERT INTO `menu_admin` (`menu_admin_kode`, `menu_kode`, `admin_level_kode`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 7, 1),
(14, 79, 1),
(16, 84, 1),
(24, 80, 1),
(30, 11, 1),
(31, 13, 1),
(32, 16, 1),
(33, 17, 1),
(34, 18, 1),
(35, 19, 1),
(36, 94, 1),
(96, 96, 1),
(102, 9, 5),
(111, 1, 2),
(112, 79, 2),
(210, 95, 1),
(211, 9, 2),
(212, 3, 2),
(213, 84, 2),
(214, 79, 3),
(215, 84, 3),
(217, 98, 1),
(218, 99, 1),
(219, 100, 1),
(220, 101, 1),
(221, 102, 1),
(222, 103, 1),
(223, 104, 1),
(224, 105, 1),
(225, 106, 1),
(226, 107, 1),
(227, 108, 1),
(228, 109, 1),
(229, 110, 1),
(230, 111, 1),
(231, 157, 1),
(232, 158, 1),
(233, 12, 1),
(234, 98, 2),
(235, 100, 2),
(236, 101, 2),
(237, 103, 2),
(249, 105, 2),
(250, 108, 2),
(251, 9, 4),
(252, 79, 4),
(254, 98, 4),
(255, 100, 4),
(256, 84, 4),
(257, 101, 4),
(258, 103, 4),
(259, 104, 4),
(260, 105, 4),
(261, 108, 4),
(262, 1, 4),
(263, 3, 4),
(264, 160, 1),
(265, 161, 1),
(266, 97, 1),
(267, 162, 1),
(268, 173, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mitra_kerja`
--

CREATE TABLE `mitra_kerja` (
  `mitra_id` int(5) NOT NULL,
  `mitra_nama` varchar(50) NOT NULL,
  `mitra_gambar` text NOT NULL,
  `mitra_link` varchar(250) NOT NULL,
  `mitra_waktu` datetime NOT NULL,
  `jenis_produk` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mitra_kerja`
--

INSERT INTO `mitra_kerja` (`mitra_id`, `mitra_nama`, `mitra_gambar`, `mitra_link`, `mitra_waktu`, `jenis_produk`) VALUES
(1, 'test1', '8e80e956a8d0bf85b2ff7201e3c8256a.jpg', 'test', '2021-01-05 00:00:00', 1),
(2, 'test2', '043d7da2260d12544d9a445354976e02.png', 'test2', '2021-01-05 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `pesan_kode` int(11) NOT NULL,
  `pesan_pengirim` varchar(50) NOT NULL,
  `pesan_subjek` varchar(255) NOT NULL,
  `pesan_email` varchar(100) NOT NULL,
  `pesan_isi` text NOT NULL,
  `pesan_datetime` datetime NOT NULL,
  `pesan_read` enum('Y','N') NOT NULL DEFAULT 'N',
  `pesan_type` enum('I','O') NOT NULL,
  `pesan_status` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`pesan_kode`, `pesan_pengirim`, `pesan_subjek`, `pesan_email`, `pesan_isi`, `pesan_datetime`, `pesan_read`, `pesan_type`, `pesan_status`) VALUES
(3, 'nava gia ginasta', 'Kontak Kami', 'navagiaginasta@gmail.com', 'Bagus :)', '2015-09-01 13:06:32', 'N', 'I', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(3) NOT NULL,
  `nama_produk` varchar(90) NOT NULL,
  `deskripsi_indo` text NOT NULL,
  `deskripsi_ing` text NOT NULL,
  `image` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi_indo`, `deskripsi_ing`, `image`) VALUES
(1, 'BANGGA e-TILA', 'Secara khusus, manfaat yang dapat dirasakan oleh pengguna dengan mengimplementasikan BANGGA e-TILA antara lain adalah :\r\n\r\nAuditor Intern dan Manajemen Audit Intern Sebagai alat bantu pihak pengawasan internal untuk menjalankan aktivitas, koordinasi dan dokumentasi hasil pemeriksaan secara terotomasi dan tersentralisasi dengan tetap mengacu kepada standar yang berlaku. Selain itu BANGGA e-TILA juga berfungsi sebagai media komunikasi antara auditor dan auditee berkaitan dengan tindak lanjut atas setiap rekomendasi hasil pemeriksaan.\r\n\r\nAudite Sebagai alat bantu untuk melaporkan aktivitas dan status tindak lanjut atas setiap rekomendasi hasil pemeriksaan, sekaligus sebagai alat bantu untuk memantau efektivitas pengendalian intern pada Auditee yang terkait.\r\n\r\nManajemen Pusat Sebagai media utama untuk melakukan koordinasi dengan fungsi pemeriksaan intern, terkait dengan pemantauan atas efektivitas dan efisiensi pelaksanaan pemeriksaan intern.\r\n\r\nAuditor Ekstern Dimungkinkan untuk digunakan sebagai alat bantu guna melakukan koordinasi dan komunikasi dengan fungsi pemeriksaan intern, berkaitan dengan lingkup penugasan audit, temuan dan rekomendasi audit, dengan memberikan hak akses sementara untuk melihat hasil pemeriksaan.', ' In particular, the benefits that can be felt by users by implementing BANGGA e-TILA include:\r\n\r\nInternal Auditor and Internal Audit Management As a tool for internal control parties to carry out activities, coordination and documentation of audit results in an automated and centralized manner while still referring to applicable standards. In addition, BANGGA e-TILA also functions as a medium of communication between the auditors and the auditee regarding follow-up on any recommendations on the results of the examination.\r\n\r\nAudite As a tool for reporting activities and follow-up status on each recommendation of examination results, as well as a tool for monitoring the effectiveness of internal control in the related Auditee.\r\n\r\nCentral Management As the main media to coordinate with the internal audit function, related to monitoring the effectiveness and efficiency of the internal examination.\r\n\r\nExternal Auditor It is possible to use it as a tool to coordinate and communicate with the internal audit function, relating to the scope of audit assignments, audit findings and recommendations, by providing temporary access rights to view the audit results.', '746a6d567181510e8089ee191c1f0c38.png'),
(2, 'BANGGA e-Risk', ' Manajemen resiko yang dilaksanakan secara efektif dan wajar dapat memberikan manfaat bagi perusahaan/korporasi, yaitu :\r\n\r\n? Membantu pencapaian tujuan perusahaan.\r\n\r\n? Perusahaan memiliki ukuran kuat sebagai pijakan dalam mengambil setiap keputusan sehingga para manajer dan stakeholder menjadi lebih berhati hati prudent dan selalu menempatkan ukuran ukuran dalam berbagai keputusan.\r\n\r\n? Menghindari biaya biaya yang mengejutkan, karena perusahaan mengidentifikasi dan mengelola risiko yang tidak diperlukan, termasuk menghindari biaya dan waktu yang dihabiskan dalam suatu masalah\r\n\r\n? Meningkatkan akuntabilitas dan corporate governance.\r\n\r\n? Mengubah pandangan terhadap risiko menjadi lebih terbuka, ada toleransi terhadap mistakes tapi tidak terhadap hiding errors Perubahan pandangan ini memungkinkan perusahaan belajar dari kesalahan masa lalunya untuk terus memperbaiki kinerjanya', ' Risk management that is carried out effectively and fairly can provide benefits for the company / corporation, namely:\r\n\r\n? Help achieve company goals.\r\n\r\n? The company has a strong measure as a basis for making every decision so that managers and stakeholders become more prudent and always place the measure in various decisions.\r\n\r\n? Avoid shocking costs, as companies identify and manage unnecessary risks, including avoiding costs and time spent on problems\r\n\r\n? Improve accountability and corporate governance.\r\n\r\n? Changing the view of risk to be more open, there is tolerance for mistakes but not against hiding errors This change in perspective allows a company to learn from its past mistakes to continuously improve its performance\r\n', 'ff41cfaf7b36fbdd687ea0561a7a34c3.png'),
(3, 'BANGGA e-WBS', ' System yang cepat, efektif dan efisien bagi masyarakat untuk mengadukan penyimpangan dan indikasi TIPIKOR yang terjadi di Organisasi secara on-line 24 jam.\r\n\r\nSystem yang menjamin kerahasiaan identitas pelapor.\r\n\r\nMemudahkan Aparat Pengawasan Internal Pemerintah (APIP) atau Unit Internal Audit dalam memantau aduan masyarakat secara real time.\r\n\r\nMemudahkan APIP atau Unit Internal Audit dalam melakukan penelusuran “Status Tindak Lanjut” atas setiap aduan masyarakat.\r\n\r\nMenyajikan Laporan atau Rekapitulasi untuk pimpinan secara cepat dan akurat dalam bentuk tabel dan grafik yang menggambarkan jumlah pengaduan masyarakat yang dapat dilihat per Satuan Kerja (satker), per Provinsi, per Tahun atau per Jenis Aduan.', ' A fast, effective and efficient system for the public to complain about irregularities and indications of TIPIKOR that occur in the Organization on-line 24 hours.\r\n\r\nA system that ensures the confidentiality of the reporter\'s identity.\r\n\r\nMake it easy for the Government Internal Supervisory Apparatus (APIP) or the Internal Audit Unit to monitor public complaints in real time.\r\n\r\nMake it easy for APIP or the Internal Audit Unit to trace “Follow-up Status” for every public complaint.\r\n\r\nPresenting reports or recapitulations for leaders quickly and accurately in the form of tables and graphs that depict the number of public complaints that can be seen per work unit (satker), per province, per year or per type of complaint.', 'ed2f15913cc67a10957bc071464d968f.PNG'),
(4, 'BANGGA KMS', 'KMS adalah system informasi berbasis pengetahuan yang mendukung penciptaan,pengaturan, dan penyebaran dari pengetahuan program kepada pegawai dan pejabat struktural dari sebuah organisasi/perusahaan.\r\n\r\nKnowledge Management berguna untuk pendokumentasian pengetahuan, sehingga dapat menjadi keunggulan kompetitif bagi organisasi/perusahaan.', ' KMS is a knowledge-based information system that supports the creation, organization, and dissemination of program knowledge to employees and structural officers of an organization / company.\r\n\r\nKnowledge Management is useful for documenting knowledge, so that it can be a competitive advantage for organizations / companies.', 'a027738a27b8d647ea16c284ade1d237.PNG'),
(5, 'BANGGA e-Office', ' Melalui semua surat yang dibuat, dikirimkan ataupun diterima, terekam dengan baik dalam jaringan internet/intranet sehingga akan lebih mudah untuk mencari surat yang diinginkan dalam waktu tertentu.\r\n\r\nE-OFFICE juga menghemat penggunaan sumber daya, seperti waktu dan biaya karena semua surat yang ada disimpan dan dibuat secara elektronik.\r\n\r\nMelalui E-OFFICE, instansi publik juga akan menghemat waktu dan biaya dalam penyampaian surat-menyurat antar stakeholders.\r\n\r\nDapat diakses dengan mudah tanpa ada batasan waktu dan tempat dan dapat diakses oleh banyak user dalam satu waktu.', 'Through all letters that are made, sent or received, recorded properly on the internet / intranet network so that it will be easier to find the desired letter within a certain time.\r\n\r\nE-OFFICE also saves the use of resources, such as time and costs because all mail is stored and created electronically.\r\n\r\nThrough E-OFFICE, public agencies will also save time and money in delivering correspondence between stakeholders.\r\n\r\nCan be accessed easily without limitation of time and place and can be accessed by many users at one time.', '2e11f1112faacf7e527e21983f66d49c.PNG'),
(6, 'BANGGA KPKU', ' https://banggasolution.com/kpku/', ' https://banggasolution.com/kpku/', 'f044f5b36ad9e865ebfc4f477aead697.png');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('0f30c0056a72e6c120ae90c8b197b747', '103.105.28.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609144782, ''),
('1d68cee9cf09a1a8038bd368c4857da2', '103.233.88.224', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 OPR/7', 1609145689, 'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"admin_user\";s:5:\"admin\";s:10:\"admin_nama\";s:4:\"Reza\";s:11:\"admin_level\";s:1:\"1\";s:9:\"logged_in\";b:1;}'),
('6debd79e2e9f28f5f9922480a847786a', '103.233.88.224', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609140670, ''),
('9ac0f5fe900c215773acd71d9b9720ae', '54.36.149.29', 'Mozilla/5.0 (compatible; AhrefsBot/7.0; +http://ahrefs.com/robot/)', 1609137494, ''),
('fe4fca90205d25af257e1af98d12a002', '114.5.209.32', 'Mozilla/5.0 (Linux; Android 10; M2003J15SC) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.101 Mobile Safari/53', 1609140030, '');

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `slide_id` int(5) NOT NULL,
  `slide_judul` varchar(100) NOT NULL,
  `slide_gambar` varchar(100) NOT NULL,
  `slide_deskripsi` text NOT NULL,
  `slide_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`slide_id`, `slide_judul`, `slide_gambar`, `slide_deskripsi`, `slide_waktu`) VALUES
(6, 'Slide2', '1439110065-slide2.JPG', '<p>\r\n	Slide2</p>\r\n', '2015-08-09 14:52:03'),
(7, 'Slide1', '1439109241-slide1.jpg', '<p>\r\n	Slide1</p>\r\n', '2015-08-09 15:34:01'),
(8, 'Slide3', '1441171313-slide3.JPG', '<p>\r\n	Slide3</p>\r\n', '2015-09-02 12:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `statis`
--

CREATE TABLE `statis` (
  `statis_id` int(5) NOT NULL,
  `statis_judul` varchar(100) NOT NULL,
  `statis_deskripsi` text NOT NULL,
  `statis_gambar` varchar(100) NOT NULL,
  `statis_status` enum('N','Y') NOT NULL,
  `statis_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(5) NOT NULL,
  `tag_judul` varchar(50) NOT NULL,
  `tag_seo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_judul`, `tag_seo`) VALUES
(1, 'Teknologii', 'teknologi'),
(2, 'Bencana Alam', 'bencana-alam'),
(3, 'Business', 'business'),
(4, 'Alibaba', 'alibaba'),
(5, 'Gadged', 'gadged'),
(6, 'Networking', 'networking'),
(7, 'Juniper', 'juniper'),
(8, 'Internet', 'internet'),
(9, '5G', '5g'),
(10, 'Digital', 'digital'),
(11, 'Googlee', 'google'),
(12, 'Fotografi', 'fotografi'),
(13, 'Mobile', 'mobile'),
(14, 'Android', 'android'),
(15, 'banggasolution', 'banggasolution'),
(16, 'riskmanagementsystem', 'riskmanagementsystem'),
(17, 'auditmanagementsystem', 'auditmanagementsystem'),
(18, 'itdeveloper', 'itdeveloper'),
(19, 'itconsultant', 'itconsultant'),
(20, 'itinfrastructure', 'itinfrastructure'),
(21, 'softwaredevelopment', 'softwaredevelopment');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `dari` text NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id`, `dari`, `deskripsi`, `image`) VALUES
(302, 'danangg', 'bagus', '0187f768766849605ab56bc45dcb1a7b.png');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonial_id` int(5) NOT NULL,
  `testimonial_nama` varchar(100) NOT NULL,
  `testimonial_sumber` varchar(100) NOT NULL,
  `testimonial_kerja` varchar(200) NOT NULL,
  `testimonial_jabatan` varchar(200) NOT NULL,
  `testimonial_deskripsi` text NOT NULL,
  `testimonial_gambar` varchar(100) NOT NULL,
  `testimonial_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`testimonial_id`, `testimonial_nama`, `testimonial_sumber`, `testimonial_kerja`, `testimonial_jabatan`, `testimonial_deskripsi`, `testimonial_gambar`, `testimonial_waktu`) VALUES
(3, 'Ali Abdul Wahid', 'Masih Kuliah', 'CV.Nava Teknologi', 'Programmer', 'Poltekpos Indonesia memiliki kampus nyaman untuk belajar, fasilitas lengkap dan dosen-dosen yg ahli di bidangnya. Terima kasih, selesai wisuda saya langsung mendapat pekerjaan.', '1440387703-11206097_907674959270914_6026589404761839948_n.jpg', '2015-08-24 10:41:43'),
(4, 'Mochammad Uki', 'Masih Kuliah', 'CV.Nava Teknologi', 'Web Design', 'Saya merasa senang bisa menjadi Mahasiswa Politeknik Pos Indonesia. Mendapat fasilitas pembelajaran yang baik. Tidak hanya Menerima Pengetahuan Secara Teori, bahkan menerima pengetahuan dari pengalaman-pengalaman para dosen. Sehingga kita siap menghadapi dunia kerja.\r\n', '1440388399-1001594_419773781456187_112239529_n.jpg', '2015-08-24 10:53:19'),
(5, 'Nava Gia Ginasta', 'Masih Kuliah', 'KOMINFO', 'Programmer Analyst & Project Leader', 'Politeknik Pos Indonesia memiliki kampus yang sangat memadai dibandingkan dengan politeknik yang lain. Proses belajar mengajar jadi lebih nyaman. Materi kuliahnya pun bagus khususnya untuk jurusan Teknik Informatika. Dari tahun ke tahun ada perubahan kurikulum yang mengikuti perkembangan IT. Dengan sistem seperti ini, mahasiswa tidak akan ketinggalan informasi tentang perkembangan IT, dan menyiapkan mahasiswanya untuk siap bersaing di dunia kerja.\r\n', '1440388490-NAVA_FOTO_500MB.jpg', '2015-08-24 10:54:50'),
(6, 'Septian Cahya Diningrat', 'Masih Kuliah', 'CV.Sumedang FC', 'Direktur', 'Program Studi Teknik Informatika Politeknik Pos Indonesia telah memberikan banyak pengetahuan kepada saya baik dalam teori maupun praktek. Lulusan Poltek Pos dituntut untuk mampu bersaing dalam dunia kerja sehingga tidak tetinggal dari lulusan sarjana universitas lain meskipun lulusan diploma. Dosen Poltekpos juga sangat berkualitas dan profesional baik dosen dari Poltek Pos sendiri Maupun Dosen Luar (Tamu) dari Kalangan praktisi atau Universitas lain. Selama menempuh pendidikan saya diberi kesempatan untuk langsung terjun ke dunia kerja, baik berupa proyek maupun kerja praktek. Selain itu saya juga mendapat banyak manfaat dari kerja sama pihak Poltek Pos dengan berbagai vendor ternama seperti Microsoft, Oracle, dan lain-lain, baik berupa pelatihan, seminar, kunjungan industri maupun yang lain. Suatu hal yang cukup penting dan sangat menguntungkan bagi saya yang tidak saya dapatkan dari tempat kuliah saya sebelum masuk ke Poltek Pos. [hide]', '1441027340-septian.jpg', '2015-08-31 20:22:20'),
(7, 'Anggi Sholihatus Sadiah', 'Masih Kuliah', 'Pemda Kab.Subang', 'Sekretaris Daerah', 'seneng banget dah bisa kuliah di politeknik pos indonesia jur tehnik informatika, banyak dapet ilmu2 yg bermanfaat di dunia kerja loch...\r\nTempat kuliahnya juga nyaman banget buat belajar...\r\nDosennya juga ga kalah okey ..\r\nGa lupa selalu Bikin kangen kuliah dsana lagi ney..\r\nDan tak terlupakan dah dpt banyak banget pengalaman selama kuliah di poltekpos pokoknya...\r\n', '1441027469-anggi.jpg', '2015-08-31 20:24:29'),
(8, 'Rizki Fadillah D\'kenjie', 'Masih Kuliah', 'Riau Hutan, Tbk', 'Manager', 'Saya adalah salah satu alumni dari Politeknikpos Indonesia dari jurusan Teknik Informatika. Poltekpos adalah kampus yang dibangun sangat kondusif bagi mereka yang ingin pandai dan meraih sukses. Dan yang membuat saya merasa nyaman adalah fasilitas laboratorium yang sangat mendukung, tim pengajar yang professional dan berkualitas di bidangnya, lingkungan yang bersih, dan kekeluargaan yang tercipta antara dosen dan mahasiswanya. Ilmu yang saya dapatkan di Poltekpos mampu mengantar saya untuk siap bersaing serta memulai langkah sukses.', '1441027729-rizki.jpg', '2015-08-31 20:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `ip_address` varchar(255) NOT NULL,
  `dari` varchar(90) NOT NULL,
  `os` varchar(50) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`ip_address`, `dari`, `os`, `browser`, `date`) VALUES
('::1', 'JakartaIndonesia', 'Windows 10', 'Opera 73.0.3856.260', '0000-00-00'),
('::1', 'JakartaIndonesia', 'Windows 10', 'Opera 73.0.3856.260', '0000-00-00'),
('::1', 'Jakarta Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '0000-00-00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '0000-00-00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04 13:27:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04 15:44:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04 17:01:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04 23:19:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-04 23:53:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 05:36:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 05:37:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:32:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:36:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:37:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:39:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:43:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 06:44:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 07:08:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 07:09:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 07:09:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 07:10:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 07:10:39'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:27:20'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:50:09'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:51:10'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:51:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:54:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 09:58:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:03:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:04:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:09:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:10:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:12:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:14:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:15:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:19:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:20:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:21:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:31:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:32:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:34:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:34:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:34:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:36:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:42:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:42:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:45:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:46:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:48:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:48:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:49:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:49:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:49:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:51:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:52:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 10:56:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:03:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:03:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:10:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:13:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:14:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 11:15:12'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 12:49:59'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 12:51:17'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 12:56:53'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 12:56:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 13:43:09'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 13:45:05'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 13:46:24'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:05:29'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:06:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:08:19'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:12:50'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:13:44'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:14:06'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:23:38'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:23:59'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:25:30'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:26:21'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:26:38'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:37:13'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:39:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:39:56'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:40:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:41:20'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:42:31'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:42:48'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:43:22'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:44:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:45:16'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:45:37'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:46:24'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:47:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:47:58'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:48:44'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:49:17'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:51:49'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:52:30'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:53:22'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:55:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:56:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:57:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:58:12'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:58:51'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:59:01'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 14:59:21'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:00:14'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:01:39'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:02:07'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:00'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:34'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:35'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:35'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:05:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:06:31'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:06:59'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:10:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:10:40'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:19:04'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:20:58'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:21:41'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:22:18'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:22:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:23:26'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:23:49'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:24:38'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:25:51'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:26:10'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:27:41'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:30:05'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:30:19'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:34:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:39:04'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:40:56'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:41:11'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:41:31'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:42:27'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:42:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:44:00'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:45:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:45:31'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:48:58'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:49:14'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:49:45'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:51:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:52:04'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:54:21'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 15:57:34'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 21:42:21'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:01:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:01:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:02:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:03:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:03:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:04:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:04:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:05:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:07:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:12:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:13:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:13:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:14:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:18:07'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:18:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:21:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:22:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:24:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:25:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:25:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:26:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:27:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:27:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:28:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:28:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:29:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:29:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:30:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:30:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:30:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:31:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:31:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:32:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:32:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:32:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:33:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:33:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:33:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:45:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:45:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:45:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:54:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 22:57:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:05:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:06:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:06:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:07:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:08:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:09:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:10:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:11:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:11:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:12:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:13:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:14:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:15:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:16:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:16:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:17:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:17:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:18:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:19:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:19:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:20:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:20:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:21:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:22:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:22:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:22:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:23:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:24:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:24:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:25:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:25:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:26:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:27:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:28:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:30:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:33:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:35:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:36:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:37:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:38:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:38:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:39:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:40:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:40:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:41:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:42:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:48:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:49:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:49:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:50:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:51:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-05 23:55:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:04:21'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:05:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:05:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:11:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:30:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:32:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:32:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:33:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:34:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:34:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:34:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:35:08'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:37:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:41:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:44:12'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:44:38'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:50:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:51:17'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:51:41'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:52:50'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:53:34'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:55:03'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 08:58:07'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:01:15'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:04:24'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:06:46'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:07:17'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:09:11'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:09:33'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 09:38:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:01:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:03:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:07:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:09:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:11:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:11:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:14:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:15:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:17:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:18:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:19:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:20:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:20:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:27:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:34:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:35:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:36:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:36:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:36:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:37:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:37:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:38:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:39:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:44:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:45:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:51:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:57:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 13:58:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:02:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:04:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:05:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:10:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:12:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:16:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:16:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:24:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:25:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:36:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:37:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:44:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:45:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:55:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:57:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 14:59:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:00:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:01:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:02:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:02:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:03:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:41:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:42:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:42:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:46:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 15:48:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:30:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:35:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:38:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:39:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:40:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:43:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:43:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:46:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:46:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:54:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:55:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:57:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:57:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:58:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 19:59:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:14:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:22:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:23:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:26:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:27:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:29:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:30:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:32:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:33:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 20:35:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:28:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:28:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:31:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:36:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:37:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:47:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:49:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:49:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 22:50:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:01:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:01:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:02:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:03:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:03:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:04:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:04:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:05:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:13:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:13:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:13:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:14:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:21:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:33:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:43:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-06 23:44:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:01:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:02:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:13:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:26:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:28:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:28:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:28:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:30:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:31:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:31:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:32:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:34:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:49:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:49:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:49:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:49:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:50:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:50:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:50:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:51:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:51:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:51:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:52:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:53:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:54:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 00:55:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:28:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:41:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:42:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:45:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:46:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 07:55:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:01:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:02:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:02:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:03:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:04:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:05:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:10:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:10:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:14:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:14:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:15:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:16:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:16:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:16:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:17:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:29:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:30:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:30:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:36:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:39:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:43:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:43:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:44:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 08:44:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:37:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:41:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:41:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:43:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:44:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:45:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:45:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:46:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:47:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:48:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:48:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:52:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:54:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:54:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:54:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:59:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-07 09:59:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:44:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:47:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:49:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:52:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:57:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 09:57:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:17:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:37:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:38:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:38:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:41:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 10:42:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:20:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:21:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:21:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:21:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:22:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:23:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:23:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:24:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:24:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:26:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:30:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:31:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:31:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 11:31:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 13:02:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.260', '2021-01-08 13:11:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_user`),
  ADD KEY `admin_level_kode` (`admin_level_kode`);

--
-- Indexes for table `admin_level`
--
ALTER TABLE `admin_level`
  ADD PRIMARY KEY (`admin_level_kode`);

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`agenda_id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `album_galeri`
--
ALTER TABLE `album_galeri`
  ADD PRIMARY KEY (`galeri_id`),
  ADD KEY `album_id` (`album_id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`berita_id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`fasilitas_id`);

--
-- Indexes for table `galeri_video`
--
ALTER TABLE `galeri_video`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`identitas_id`);

--
-- Indexes for table `join_event`
--
ALTER TABLE `join_event`
  ADD PRIMARY KEY (`join_id`,`agenda_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`komentar_id`),
  ADD KEY `berita_id` (`berita_id`);

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`management_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_kode`);

--
-- Indexes for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD PRIMARY KEY (`menu_admin_kode`),
  ADD KEY `menu_kode` (`menu_kode`),
  ADD KEY `admin_level_kode` (`admin_level_kode`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitra_kerja`
--
ALTER TABLE `mitra_kerja`
  ADD PRIMARY KEY (`mitra_id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`pesan_kode`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `statis`
--
ALTER TABLE `statis`
  ADD PRIMARY KEY (`statis_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_level`
--
ALTER TABLE `admin_level`
  MODIFY `admin_level_kode` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `agenda_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `album_galeri`
--
ALTER TABLE `album_galeri`
  MODIFY `galeri_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `berita_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2530;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `download_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `fasilitas_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galeri_video`
--
ALTER TABLE `galeri_video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `identitas_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `join_event`
--
ALTER TABLE `join_event`
  MODIFY `join_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `komentar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `management_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `menu_admin`
--
ALTER TABLE `menu_admin`
  MODIFY `menu_admin_kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mitra_kerja`
--
ALTER TABLE `mitra_kerja`
  MODIFY `mitra_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `pesan_kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `slide_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `statis`
--
ALTER TABLE `statis`
  MODIFY `statis_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonial_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
